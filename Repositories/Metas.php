<?php
/**
 * Created by IntelliJ IDEA.
 * User: simonnguyen
 * Date: 3/17/16
 * Time: 10:28 AM
 */

namespace Weelis\Repository\Repositories;

use Weelis\Repository\Entities\Meta;
use Weelis\Repository\Eloquent\BaseRepository;

class Metas extends BaseRepository
{
    public function model() {
        return Meta::class;
    }
}