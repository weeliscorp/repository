<?php
/**
 * Created by IntelliJ IDEA.
 * User: simonnguyen
 * Date: 3/24/16
 * Time: 10:30 AM
 */

namespace Weelis\Repository\Builder;

use Closure;
use Doctrine\DBAL\Exception\InvalidFieldNameException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Str;
use Jenssegers\Mongodb\Eloquent\Model;
use Weelis\Plugin\Exceptions\InvalidArgumentException;

class MorphBuilder extends Builder
{
    /**
     * Add a relationship count / exists condition to the query.
     *
     * @param  string $relation
     * @param  string $boolean
     * @param  \Closure|null $callback
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function doesntHave($relation, $boolean = 'and', Closure $callback = null, $morphTypes = null)
    {
        return $this->has($relation, '<', 1, $boolean, $callback, $morphTypes);
    }

    /**
     * Add a relationship count / exists condition to the query.
     *
     * @param  string $relation
     * @param  string $operator
     * @param  int $count
     * @param  string $boolean
     * @param  \Closure|null $callback
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function has($relation, $operator = '>=', $count = 1, $boolean = 'and', Closure $callback = null, $morphTypes = null, $morphCall = false)
    {
        if (strpos($relation, '.') !== false) {
            return $this->hasNested($relation, $operator, $count, $boolean, $callback);
        }

        // $relation = $this->getHasRelationQuery($relation);
        // Check for whereHas call with morphTo relation
        if (!is_null($morphTypes) && $morphCall === false) {
            return $this->hasMorphed($relation, $operator, $count, $boolean, $callback, $morphTypes);
        }
        $relation = $this->getHasRelationQuery($relation, $morphTypes);

        // If we only need to check for the existence of the relation, then we can
        // optimize the subquery to only run a "where exists" clause instead of
        // the full "count" clause. This will make the query run much faster.
        //$queryType = 'getRelationCountQuery';
        $queryType = $this->shouldRunExistsQuery($operator, $count)
            ? 'getRelationQuery' : 'getRelationCountQuery';
        $query = $relation->{$queryType}($relation->getRelated()->newQuery(), $this);
        if ($callback) {
            call_user_func($callback, $query);
        }
        if ($relation->getModel() instanceof Model) { // Fix wherehas morphto
            return $this->addHasWhereMongo($query, $relation, $operator, $count, $boolean);
        }

        return $this->addHasWhere($query, $relation, $operator, $count, $boolean);
    }

    protected function addHasWhereMongo(Builder $hasQuery, Relation $relation, $operator, $count, $boolean)
    {
        $query = $hasQuery->getQuery();
        // Get the number of related objects for each possible parent.
        $relationCount = array_count_values($query->pluck($relation->getHasCompareKey())->all());
        // Remove unwanted related objects based on the operator and count.
        $relationCount = array_filter($relationCount, function ($counted) use ($count, $operator) {
            // If we are comparing to 0, we always need all results.
            if ($count == 0) {
                return true;
            }

            switch ($operator) {
                case '>=':
                case '<':
                    return $counted >= $count;
                case '>':
                case '<=':
                    return $counted > $count;
                case '=':
                case '!=':
                    return $counted == $count;
            }
        });

        // If the operator is <, <= or !=, we will use whereNotIn.
        $not = in_array($operator, ['<', '<=', '!=']);

        // If we are comparing to 0, we need an additional $not flip.
        if ($count == 0) {
            $not = !$not;
        }

        // All related ids.
        $relatedIds = array_keys($relationCount);

        // Add whereIn to the query.
        return $this->whereIn($this->model->getKeyName(), $relatedIds, $boolean, $not);
    }



    public function hasMorphed($relation, $operator = '>=', $count = 1, $boolean = 'and', Closure $callback = null, $morphTypes = null)
    {
        if (!is_array($morphTypes)) {
            $morphTypes = [$morphTypes];
        }
        // Wrap again to make sure the deleted_at = null query
        // applies to all of these has queries
        // This isn't a problem with one sub query,
        // but it is when using multiple in one whereHas
        return $this->where(function ($query) use ($morphTypes, $relation, $operator, $count, $callback) {
            foreach ($morphTypes as $type) {
                $query->has($relation, $operator, $count, 'or', $callback, $type, true);
            }
        }, null, null, $boolean);
    }

    /**
     * Add a relationship count / exists condition to the query with an "or".
     *
     * @param  string $relation
     * @param  string $operator
     * @param  int $count
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function orHas($relation, $operator = '>=', $count = 1, $morphTypes = null)
    {
        return $this->has($relation, $operator, $count, 'or', null, $morphTypes);
    }

    /**
     * Add a relationship count / exists condition to the query with where clauses and an "or".
     *
     * @param  string $relation
     * @param  \Closure $callback
     * @param  string $operator
     * @param  int $count
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function orWhereHas($relation, Closure $callback, $operator = '>=', $count = 1, $morphTypes = null)
    {
        return $this->has($relation, $operator, $count, 'or', $callback, $morphTypes);
    }
}