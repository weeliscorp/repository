<?php namespace Weelis\Repository\Transformer;

use League\Fractal\TransformerAbstract;
use Weelis\Repository\Contracts\Transformable;

/**
 * Class ModelTransformer
 * @package Weelis\Repository\Transformer
 */
class ModelTransformer extends TransformerAbstract
{
    public function transform(Transformable $model)
    {
        return $model->transform();
    }
}