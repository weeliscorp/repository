# Citinet project

## Table of Contents

- [Methods](#markdown-header-methods)
    - [RepositoryInterface](#markdown-header-weelisrepositorycontractsrepositoryinterface)
    - [RepositoryCriteriaInterface](#markdown-header-weelisrepositorycontractsrepositorycriteriainterface)
    - [CacheableInterface](#markdown-header-weelisrepositorycontractscacheableinterface)
    - [PresenterInterface](#markdown-header-weelisrepositorycontractspresenterinterface)
    - [Presentable](#markdown-header-weelisrepositorycontractspresentable)
    - [CriteriaInterface](#markdown-header-weelisrepositorycontractscriteriainterface)
    - [Transformable](#markdown-header-weelisrepositorycontractstransformable)
- [Usage](#markdown-header-usage)
	- [Create a Model](#markdown-header-create-a-model)
	- [Create a Repository](#markdown-header-create-a-repository)
	- [Generators](#markdown-header-generators)
	- [Use methods](#markdown-header-use-methods)
	- [Create a Criteria](#markdown-header-create-a-criteria)
	- [Using the Criteria in a Controller](#markdown-header-using-the-criteria-in-a-controller)
	- [Using the RequestCriteria](#markdown-header-using-the-requestcriteria)
- [Cache](#markdown-header-cache)
    - [Usage](#markdown-header-cache-usage)
    - [Config](#markdown-header-cache-config)
- [Validators](#markdown-header-validators)
    - [Using a Validator Class](#markdown-header-using-a-validator-class)
        - [Create a Validator](#markdown-header-create-a-validator)
        - [Enabling Validator in your Repository](#markdown-header-enabling-validator-in-your-repository-1)
    - [Defining rules in the repository](#markdown-header-defining-rules-in-the-repository)
- [Presenters](#markdown-header-presenters)
    - [Fractal Presenter](#markdown-header-fractal-presenter)
        - [Create a Fractal Presenter](#markdown-header-create-a-presenter)
        - [Model Transformable](#markdown-header-implement-interface)
    - [Enabling in your Repository](#markdown-header-enabling-in-your-repository-1)
- [Helper Traits](#markdown-header-helper-traits)
    - [Transactional Helper](#markdown-header-transactional-helper)
    - [DataTable Helper](#markdown-header-datatable-helper)
    - [Meta Helper](#markdown-header-meta-helper)
- [SphinxQL library for Laravel 5.1](#markdown-header-sphinxql-library-for-laravel-51)
    - [RT (Real-Time) Indexes in Sphinx](#markdown-header-rt-real-time-indexes-in-sphinx)
    - [Query Builder Documentation](#markdown-header-query-builder-documentation)
    - [Misc Usage Tips](#markdown-header-misc-usage-tips)


## Methods

### Weelis\Repository\Contracts\RepositoryInterface

- all($columns = array('*'))
- count()
- first($columns = array('*'))
- paginate($limit = null, $columns = ['*'])
- find($id, $columns = ['*'])
- findByField($field, $value, $columns = ['*'])
- findWhere(array $where, $columns = ['*'])
- findWhereIn($field, array $where, $columns = [*])
- findWhereNotIn($field, array $where, $columns = [*])
- firstOrCreate($attributes = [])
- when($condition, callback)
- create(array $attributes)
- update(array $attributes, $id)
- updateWhere(array $attributes)
- updateOrCreate(array $attributes, array $values = [])
- delete($id)
- deleteWhere(array $where)
- orderBy($column, $direction = 'asc')
- with(array $relations)
- hidden(array $fields)
- visible(array $fields)
- scopeQuery(Closure $scope)
- getFieldsSearchable()
- setTransform($transformfunc)
- setPresenter($presenter)
- skipPresenter($status = true)


### Weelis\Repository\Contracts\RepositoryCriteriaInterface

- pushCriteria($criteria)
- popCriteria($criteria)
- getCriteria()
- getByCriteria(CriteriaInterface $criteria)
- skipCriteria($status = true)
- getFieldsSearchable()

### Weelis\Repository\Contracts\CacheableInterface

- setCacheRepository(CacheRepository $repository)
- getCacheRepository()
- getCacheKey($method, $args = null)
- getCacheMinutes()
- skipCache($status = true)

### Weelis\Repository\Contracts\PresenterInterface

- present($data);

### Weelis\Repository\Contracts\Presentable

- setPresenter(PresenterInterface $presenter);
- presenter();

### Weelis\Repository\Contracts\CriteriaInterface

- apply($model, RepositoryInterface $repository);

### Weelis\Repository\Contracts\Transformable

- transform();
- setTransform($transformFunc);

## Usage

### Create a Model

Create your model normally, but it is important to define the attributes that can be filled from the input form data.

```php
namespace App;

class Post extends Eloquent { // or Ardent, Or any other Model Class

    protected $fillable = [
        'title',
        'author',
        ...
     ];

     ...
}
```

### Create a Repository

```php
namespace App;

use Weelis\Repository\Eloquent\BaseRepository;

class PostRepository extends BaseRepository {

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return "App\\Post";
    }
}
```

### Generators

Create your repositories easily through the generator.

#### Config

You must first configure the storage location of the repository files. By default is the "app" folder and the namespace "App".

```php
    ...
    'generator'=>[
        'basePath'=>app_path(),
        'rootNamespace'=>'App\\',
        'paths'=>[
            'models'       => 'Entities',
            'repositories' => 'Repositories',
            'interfaces'   => 'Repositories',
            'transformers' => 'Transformers',
            'presenters'   => 'Presenters',
            'validators'   => 'Validators',
            'controllers'  => 'Http/Controllers',
            'provider'     => 'RepositoryServiceProvider',
        ]
    ]
```

You may want to save the root of your project folder out of the app and add another namespace, for example

```php
    ...
     'generator'=>[
        'basePath'      => base_path('src/Lorem'),
        'rootNamespace' => 'Lorem\\'
    ]
```

Additionally, you may wish to customize where your generated classes end up being saved.  That can be accomplished by editing the `paths` node to your liking.  For example:

```php
    'generator'=>[
        'basePath'=>app_path(),
        'rootNamespace'=>'App\\',
        'paths'=>[
            'models'=>'Models',
            'repositories'=>'Repositories\\Eloquent',
            'interfaces'=>'Contracts\\Repositories',
            'transformers'=>'Transformers',
            'presenters'=>'Presenters'
            'validators'   => 'Validators',
            'controllers'  => 'Http/Controllers',
            'provider'     => 'RepositoryServiceProvider',
        ]
    ]
```

#### Commands

To generate everything you need for your Model, run this command:

```terminal
php artisan make:entity Post
```

This will create the Controller, the Validator, the Model, the Repository, the Presenter and the Transformer classes.
It will also create a new service provider that will be used to bind the Eloquent Repository with its corresponding Repository Interface.
To load it, just add this to your AppServiceProvider@register method:

```php
    $this->app->register(RepositoryServiceProvider::class);
```

You can also pass the options from the ```repository``` command, since this command is just a wrapper.

To generate a repository for your Post model, use the following command

```terminal
php artisan make:repository Post
```

To generate a repository for your Post model with Blog namespace, use the following command

```terminal
php artisan make:repository "Blog\Post"
```

Added fields that are fillable

```terminal
php artisan make:repository "Blog\Post" --fillable="title,content"
```

To add validations rules directly with your command you need to pass the `--rules` option and create migrations as well:

```terminal
php artisan make:entity Cat --fillable="title:string,content:text" --rules="title=>required|min:2, content=>sometimes|min:10"
```

The command will also create your basic RESTfull controller so just add this line into your `routes.php` file and you will have a basic CRUD:

 ```php
 Route::resource('cats', CatsController::class);
 ```

When running the command, you will be creating the "Entities" folder and "Repositories" inside the folder that you set as the default.

Done, done that just now you do bind its interface for your real repository, for example in your own Repositories Service Provider.

```php
App::bind('{YOUR_NAMESPACE}Repositories\PostRepository', '{YOUR_NAMESPACE}Repositories\PostRepositoryEloquent');
```

And use

```php
public function __construct({YOUR_NAMESPACE}Repositories\PostRepository $repository){
    $this->repository = $repository;
}
```

### Use methods

```php
namespace App\Http\Controllers;

use App\PostRepository;

class PostsController extends BaseController {

    /**
     * @var PostRepository
     */
    protected $repository;

    public function __construct(PostRepository $repository){
        $this->repository = $repository;
    }

    ....
}
```

Find all results in Repository

```php
$posts = $this->repository->all();
```

Find all results in Repository with pagination

```php
$posts = $this->repository->paginate($limit = null, $columns = ['*']);
```

Find by result by id

```php
$post = $this->repository->find($id);
```

Hiding attributes of the model

```php
$post = $this->repository->hidden(['country_id'])->find($id);
```

Showing only specific attributes of the model

```php
$post = $this->repository->visible(['id', 'state_id'])->find($id);
```

Loading the Model relationships

```php
$post = $this->repository->with(['state'])->find($id);
```

Find by result by field name

```php
$posts = $this->repository->findByField('country_id','15');
```

Find by result by multiple fields

```php
$posts = $this->repository->findWhere([
    //Default Condition =
    'state_id'=>'10',
    'country_id'=>'15',
    //Custom Condition
    ['columnName','>','10']
]);
```

Find by result by multiple values in one field

```php
$posts = $this->repository->findWhereIn('id', [1,2,3,4,5]);
```

Find by result by excluding multiple values in one field

```php
$posts = $this->repository->findWhereNotIn('id', [6,7,8,9,10]);
```

Find all using custom scope

```php
$posts = $this->repository->scopeQuery(function($query){
    return $query->orderBy('sort_order','asc');
})->all();
```

Create new entry in Repository

```php
$post = $this->repository->create( Input::all() );
```

Update entry in Repository

```php
$post = $this->repository->update( Input::all(), $id );
```

Or update base on condition

```php
$this->repository->scopeQuery(function($query){
    return $query->where('match','matched');
})->updateWhere(Input::all())
```

Delete entry in Repository

```php
$this->repository->delete($id)
```

Or delete where condition

```php
$this->repository->deleteWhere(["id" => $id, ["s", "like", "str"]])
```

### Create a Criteria

Criteria are a way to change the repository of the query by applying specific conditions according to your needs. You can add multiple Criteria in your repository.

```php

use Weelis\Repository\Contracts\RepositoryInterface;
use Weelis\Repository\Contracts\CriteriaInterface;

class MyCriteria implements CriteriaInterface {

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->where('user_id','=', Auth::user()->id );
        return $model;
    }
}
```

### Using the Criteria in a Controller

```php

namespace App\Http\Controllers;

use App\PostRepository;

class PostsController extends BaseController {

    /**
     * @var PostRepository
     */
    protected $repository;

    public function __construct(PostRepository $repository){
        $this->repository = $repository;
    }


    public function index()
    {
        $this->repository->pushCriteria(new MyCriteria1());
        $this->repository->pushCriteria(MyCriteria2::class);
        $posts = $this->repository->all();
		...
    }

}
```

Getting results from Criteria

```php
$posts = $this->repository->getByCriteria(new MyCriteria());
```

Setting the default Criteria in Repository

```php
use Weelis\Repository\Eloquent\BaseRepository;

class PostRepository extends BaseRepository {

    public function boot(){
        $this->pushCriteria(new MyCriteria());
        // or
        $this->pushCriteria(AnotherCriteria::class);
        ...
    }

    function model(){
       return "App\\Post";
    }
}
```

### Skip criteria defined in the repository

Use `skipCriteria` before any other chaining method

```php
$posts = $this->repository->skipCriteria()->all();
```

### Popping criteria

Use `popCriteria` to remove a criteria

```php
$this->repository->popCriteria(new Criteria1());
// or
$this->repository->popCriteria(Criteria1::class);
```


### Using the RequestCriteria

RequestCriteria is a standard Criteria implementation. It enables filters to perform in the repository from parameters sent in the request.

You can perform a dynamic search, filter the data and customize the queries.

To use the Criteria in your repository, you can add a new criteria in the boot method of your repository, or directly use in your controller, in order to filter out only a few requests.

####Enabling in your Repository

```php
use Weelis\Repository\Eloquent\BaseRepository;
use Weelis\Repository\Criteria\RequestCriteria;


class PostRepository extends BaseRepository {

	/**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'email'
    ];

    public function boot(){
        $this->pushCriteria(app('Weelis\Repository\Criteria\RequestCriteria'));
        ...
    }

    function model(){
       return "App\\Post";
    }
}
```

Remember, you need to define which fields from the model can be searchable.

In your repository set **$fieldSearchable** with the name of the fields to be searchable or a relation to fields.

```php
protected $fieldSearchable = [
	'name',
	'email',
	'product.name'
];
```

You can set the type of condition which will be used to perform the query, the default condition is "**=**"

```php
protected $fieldSearchable = [
	'name'=>'like',
	'email', // Default Condition "="
	'your_field'=>'condition'
];
```

####Enabling in your Controller

```php
	public function index()
    {
        $this->repository->pushCriteria(app('Weelis\Repository\Criteria\RequestCriteria'));
        $posts = $this->repository->all();
		...
    }
```

#### Example the Criteria

Request all data without filter by request

`http://Weelis.local/users`

```json
[
    {
        "id": 1,
        "name": "John Doe",
        "email": "john@gmail.com",
        "created_at": "-0001-11-30 00:00:00",
        "updated_at": "-0001-11-30 00:00:00"
    },
    {
        "id": 2,
        "name": "Lorem Ipsum",
        "email": "lorem@ipsum.com",
        "created_at": "-0001-11-30 00:00:00",
        "updated_at": "-0001-11-30 00:00:00"
    },
    {
        "id": 3,
        "name": "Laravel",
        "email": "laravel@gmail.com",
        "created_at": "-0001-11-30 00:00:00",
        "updated_at": "-0001-11-30 00:00:00"
    }
]
```

Conducting research in the repository

`http://Weelis.local/users?search=John%20Doe`

or

`http://Weelis.local/users?search=John&searchFields=name:like`

or

`http://Weelis.local/users?search=john@gmail.com&searchFields=email:=`

or

`http://Weelis.local/users?search=name:John Doe;email:john@gmail.com`

or

`http://Weelis.local/users?search=name:John;email:john@gmail.com&searchFields=name:like;email:=`

```json
[
    {
        "id": 1,
        "name": "John Doe",
        "email": "john@gmail.com",
        "created_at": "-0001-11-30 00:00:00",
        "updated_at": "-0001-11-30 00:00:00"
    }
]
```

Filtering fields

`http://Weelis.local/users?filter=id;name`

```json
[
    {
        "id": 1,
        "name": "John Doe"
    },
    {
        "id": 2,
        "name": "Lorem Ipsum"
    },
    {
        "id": 3,
        "name": "Laravel"
    }
]
```

Sorting the results

`http://Weelis.local/users?filter=id;name&orderBy=id&sortedBy=desc`

```json
[
    {
        "id": 3,
        "name": "Laravel"
    },
    {
        "id": 2,
        "name": "Lorem Ipsum"
    },
    {
        "id": 1,
        "name": "John Doe"
    }
]
```

Add relationship

`http://Weelis.local/users?with=groups`



####Overwrite params name

You can change the name of the parameters in the configuration file **config/repository.php**

### Cache

Add a layer of cache easily to your repository

#### Cache Usage

Implements the interface CacheableInterface and use CacheableRepository Trait.

```php
use Weelis\Repository\Eloquent\BaseRepository;
use Weelis\Repository\Contracts\CacheableInterface;
use Weelis\Repository\Traits\CacheableRepository;

class PostRepository extends BaseRepository implements CacheableInterface {

    use CacheableRepository;

    // Enable event for cache
    protected $skipEvents = false;

    ...
}
```

Done , done that your repository will be cached , and the repository cache is cleared whenever an item is created, modified or deleted.

#### Cache Config

You can change the cache settings in the file *config/repository.php* and also directly on your repository.

*config/repository.php*

```php
'cache'=>[
    //Enable or disable cache repositories
    'enabled'   => true,

    //Lifetime of cache
    'minutes'   => 30,

    //Repository Cache, implementation Illuminate\Contracts\Cache\Repository
    'repository'=> 'cache',

    //Sets clearing the cache
    'clean'     => [
        //Enable, disable clearing the cache on changes
        'enabled' => true,

        'on' => [
            //Enable, disable clearing the cache when you create an item
            'create'=>true,

            //Enable, disable clearing the cache when upgrading an item
            'update'=>true,

            //Enable, disable clearing the cache when you delete an item
            'delete'=>true,
        ]
    ],
    'params' => [
        //Request parameter that will be used to bypass the cache repository
        'skipCache'=>'skipCache'
    ],
    'allowed'=>[
        //Allow caching only for some methods
        'only'  =>null,

        //Allow caching for all available methods, except
        'except'=>null
    ],
],
```

It is possible to override these settings directly in the repository.

```php
use Weelis\Repository\Eloquent\BaseRepository;
use Weelis\Repository\Contracts\CacheableInterface;
use Weelis\Repository\Traits\CacheableRepository;

class PostRepository extends BaseRepository implements CacheableInterface {

    // Setting the lifetime of the cache to a repository specifically
    protected $cacheMinutes = 90;

    protected $cacheOnly = ['all', ...];
    //or
    protected $cacheExcept = ['find', ...];

    use CacheableRepository;

    ...
}
```

The cacheable methods are : all, paginate, find, findByField, findWhere, getByCriteria

### Validators

Requires [Weelis/laravel-validator](https://github.com/Weelis/laravel-validator). `composer require Weelis/laravel-validator`

Easy validation with `Weelis/laravel-validator`

[For more details click here](https://github.com/Weelis/laravel-validator)

#### Using a Validator Class

##### Create a Validator

In the example below, we define some rules for both creation and edition

```php
use \Weelis\Validator\LaravelValidator;

class PostValidator extends LaravelValidator {

    protected $rules = [
        'title' => 'required',
        'text'  => 'min:3',
        'author'=> 'required'
    ];

}
```

To define specific rules, proceed as shown below:

```php
use \Weelis\Validator\Contracts\ValidatorInterface;
use \Weelis\Validator\LaravelValidator;

class PostValidator extends LaravelValidator {

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'title' => 'required',
            'text'  => 'min:3',
            'author'=> 'required'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'title' => 'required'
        ]
   ];

}
```

##### Enabling Validator in your Repository

```php
use Weelis\Repository\Eloquent\BaseRepository;
use Weelis\Repository\Criteria\RequestCriteria;

class PostRepository extends BaseRepository {

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model(){
       return "App\\Post";
    }

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return "App\\PostValidator";
    }
}
```

#### Defining rules in the repository

Alternatively, instead of using a class to define its validation rules, you can set your rules directly into the rules repository property, it will have the same effect as a Validation class.

```php
use Weelis\Repository\Eloquent\BaseRepository;
use Weelis\Repository\Criteria\RequestCriteria;
use Weelis\Validator\Contracts\ValidatorInterface;

class PostRepository extends BaseRepository {

    /**
     * Specify Validator Rules
     * @var array
     */
     protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'title' => 'required',
            'text'  => 'min:3',
            'author'=> 'required'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'title' => 'required'
        ]
   ];

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model(){
       return "App\\Post";
    }

}
```

Validation is now ready. In case of a failure an exception will be given of the type: *Weelis\Validator\Exceptions\ValidatorException*

### Presenters

Presenters function as a wrapper and renderer for objects.

#### Fractal Presenter

Requires [Fractal](http://fractal.thephpleague.com/). `composer require league/fractal`

There are two ways to implement the Presenter, the first is creating a TransformerAbstract and set it using your Presenter class as described in the Create a Transformer Class.

The second way is to make your model implement the Transformable interface, and use the default Presenter ModelFractarPresenter, this will have the same effect.

##### Transformer Class

###### Create a Transformer using the command

```terminal
php artisan make:transformer Post
```

This wil generate the class beneath.

###### Create a Transformer Class

```php
use League\Fractal\TransformerAbstract;

class PostTransformer extends TransformerAbstract
{
    public function transform(\Post $post)
    {
        return [
            'id'      => (int) $post->id,
            'title'   => $post->title,
            'content' => $post->content
        ];
    }
}
```

###### Create a Presenter using the command

```terminal
php artisan make:presenter Post
```

The command will prompt you for creating a Transformer too if you haven't already.
###### Create a Presenter

```php
use Weelis\Repository\Presenter\FractalPresenter;

class PostPresenter extends FractalPresenter {

    /**
     * Prepare data to present
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new PostTransformer();
    }
}
```

###### Enabling in your Repository

```php
use Weelis\Repository\Eloquent\BaseRepository;

class PostRepository extends BaseRepository {

    ...

    public function presenter()
    {
        return "App\\Presenter\\PostPresenter";
    }
}
```

Or enable it in your controller with

```php
$this->repository->setPresenter("App\\Presenter\\PostPresenter");
```

###### Using the presenter after from the Model

If you recorded a presenter and sometime used the `skipPresenter()` method or simply you do not want your result is not changed automatically by the presenter.
You can implement Presentable interface on your model so you will be able to present your model at any time. See below:

In your model, implement the interface `Weelis\Repository\Contracts\Presentable` and `Weelis\Repository\Traits\PresentableTrait`

```php
namespace App;

use Weelis\Repository\Contracts\Presentable;
use Weelis\Repository\Traits\PresentableTrait;

class Post extends Eloquent implements Presentable {

    use PresentableTrait;

    protected $fillable = [
        'title',
        'author',
        ...
     ];

     ...
}
```

There, now you can submit your Model individually, See an example:

```php
$repository = app('App\PostRepository');
$repository->setPresenter("Weelis\\Repository\\Presenter\\ModelFractalPresenter");

//Getting the result transformed by the presenter directly in the search
$post = $repository->find(1);

print_r( $post ); //It produces an output as array

...

//Skip presenter and bringing the original result of the Model
$post = $repository->skipPresenter()->find(1);

print_r( $post ); //It produces an output as a Model object
print_r( $post->presenter() ); //It produces an output as array

```

You can skip the presenter at every visit and use it on demand directly into the model, for it set the `$skipPresenter` attribute to true in your repository:

```php
use Weelis\Repository\Eloquent\BaseRepository;

class PostRepository extends BaseRepository {

    /**
    * @var bool
    */
    protected $skipPresenter = true;

    public function presenter()
    {
        return "App\\Presenter\\PostPresenter";
    }
}
```

##### Model Class

###### Implement Interface

```php
namespace App;

use Weelis\Repository\Contracts\Transformable;

class Post extends Eloquent implements Transformable {
     ...
     /**
      * @return array
      */
     public function transform()
     {
         return [
             'id'      => (int) $this->id,
             'title'   => $this->title,
             'content' => $this->content
         ];
     }
}
```

###### Enabling in your Repository

`Weelis\Repository\Presenter\ModelFractalPresenter` is a Presenter default for Models implementing Transformable

```php
use Weelis\Repository\Eloquent\BaseRepository;

class PostRepository extends BaseRepository {

    ...

    public function presenter()
    {
        return "Weelis\\Repository\\Presenter\\ModelFractalPresenter";
    }
}
```

Or enable it in your controller with

```php
$this->repository->setPresenter("Weelis\\Repository\\Presenter\\ModelFractalPresenter");
```

### Skip Presenter defined in the repository

Use *skipPresenter* before any other chaining method

```php
$posts = $this->repository->skipPresenter()->all();
```

or

```php
$this->repository->skipPresenter();

$posts = $this->repository->all();
```

### Direct callback transform

```php
$this->repository->setTransform(function($item){
    return [
        "id" => $item->id,
        "name" => $item->name,
        "transforms" => $item->transform(function($item){
            return [
                "id" => $item->id,
                ...
            ];
        }),
        ...
    ];
});
```

### Helper Traits:

####Transactional Helper:
```php
<?php
// Author Repository
namespace App\Repositories;

use Weelis\Repository\Eloquent\BaseRepository;
use Weelis\Repository\Traits\EloquentTransactional;
use App\Models\Author;

class AuthorRepository extends BaseRepository
{
	use EloquentTransactional;  // <-- here

    public function getModelClass()
    {
        return Author::class;
    }
}
```

Transaction example:
```php
// ...
$repository = new AuthorRepository();

$result = $repository->transaction(function () use ($repository, $request) {
	return $repository->create($request->all());
});

// OR
try {
	$repository->beginTransaction();
	// operations ...
	$repository->commit();
} catch (\Exception $e) {
	$repository->rollback();
}
```
####DataTable Helper

The DataTable helper provide data for  http://datatables.net/

```php
<?php
// Author Repository
namespace App\Repositories;

use Weelis\Repository\Eloquent\BaseRepository;
use Weelis\Repository\Traits\DataTables;
use App\Models\Author;

class AuthorRepository extends BaseRepository
{
	use DataTables;  // <-- here

    public function getModelClass()
    {
        return Author::class;
    }
}
```

DataTable Example:
```php
<?php
// ...
use App\Repositories\AuthorRepository;

class AuthorController extends Controller
{
	protected $repository;
	public function __construct(AuthorRepository $repository)
	{
		$this->repository = $repository;
	}

	public function index()
	{
		return view('author.index');
	}

	public function getData(Request $request)
	{
		return $this->repository->dataTable($request);
	}
	// ...
}
```
DataTable View example:
```html
@extends("layouts.main")
@section('content')
<table id="example" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>Created At</th>
                <th>Updated At</th>
            </tr>
        </thead>
    </table>

<script>
$(document).ready(function() {
    $('#example').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "/authors/data"
    } );
} );
</script>
@endsection
```

#### Meta Helper

Fluent Meta Data for Eloquent Models

Eloquent Example:
```php
<?php
// ...
use Illuminate\Database\Eloquent\Model;
use Weelis\Repository\Traits\Morphable;
use Weelis\Repository\Traits\MetaEloquentTrait;

class Author extends Model
{
    use Morphable, MetaEloquentTrait;
}
```

Repository Example:
```php
<?php
// Author Repository
namespace App\Repositories;

use Weelis\Repository\Eloquent\BaseRepository;
use Weelis\Repository\Traits\MetadataTrait;
use App\Models\Author;

class AuthorRepository extends BaseRepository
{
	use MetadataTrait;  // <-- here

    public function getModelClass()
    {
        return Author::class;
    }
}
```

Create meta example:
```php
AuthorRepo::createMeta([
        'title' 			=> $title,
        'content'	        => $content
        ], $author->id);
```

Update meta example:
```php
AuthorRepo::updateMeta([
        'title' 			=> $title,
        'content'	        => $content
        ], $author->id);
```

Unset meta name example:
```php
AuthorRepo::unsetMeta([
        'title'             => $title,
        'content'           => $content
        ], $author->id);
```

Delete all meta data example:
```php
AuthorRepo::deleteMeta($author->id);
```

Access meta example:
```php
$author = AuthorRepo::withMeta()->first();
dump($author->content) //Access meta via eloquent attribute
```

Access meta object example:
```php
$meta = AuthorRepo::getMeta($author->id);
dump($meta->content) //Access meta object
```

Access meta name example:
```php
$content = AuthorRepo::getMetaByName('content', $author->id);
dump($content) //Access meta object
```

## SphinxQL library for Laravel 5.5

This is a simple library that will help you to query a sphinx search server using SphinxQL.
My main motivation for putting together this package was to interface easily
with Sphinx Real-time indexes on Laravel 4 (Updating rt indexes is ONLY possible using SphinxQL)

As an added bonus, SphinxQL is much more performant than SphinxAPI:
http://sphinxsearch.com/blog/2010/04/25/sphinxapi-vs-sphinxql-benchmark/

### RT (Real-Time) Indexes in Sphinx

The main differences between the conventional and RT indexes in Sphinx are:

 1. RT uses a "push" model. This means that The task of keeping the index in sync with your database is
delegated to your application (remember there is no "indexer" in an RT scheme). So, typically when using
an RT index, the index starts out empty and gets populated over time using SphinxQL queries sent by your application.

 2. RT uses more RAM than the conventional indexer approach. Here is an informative blog detailing why:
http://www.ivinco.com/blog/sphinx-in-action-good-and-bad-in-sphinx-real-time-indexes/
Also, for more information, be sure to read the internals of RT at :
http://sphinxsearch.com/docs/archives/1.10/rt-internals.html
	In either type of indexing strategy (conventional or RT), for best results, you are recommended to
dedicate *as much RAM* as is possible to your sphinx server. So, the additional
RAM requirement should not really deter you from using RT.

 3. A major pro for RT indexes is that they are much easier to setup and manage. There is no need for messing with
cron jobs as the "indexer" component is not used (fewer moving parts). No main-delta schemes to worry about (typically). And, ofcourse
your index is live with search data instantly!

The more recent versions of Sphinx (2.1.1+) have made enormous strides in
making RT indexes production ready:
http://sphinxsearch.com/blog/2013/01/15/realtime-index-improvements-in-2-1-1/

The current version uses sensible configuration defaults.. so you can have a
clean sphinx.conf file that takes care of the most common scenarios out of the box.
http://sphinxsearch.com/docs/current.html#sphinx-deprecations-defaults

Here is the (minimal) sphinx.conf file used in examples below (for rt indexing):
```php
index rt_test
{
    type = rt
    path = /var/lib/sphinxsearch/data/rt
    rt_field = title
    rt_field = content
    rt_attr_uint = gid
}
searchd
{
    listen                  = 9306:mysql41

    # log file, searchd run info is logged here
    # optional, default is 'searchd.log'
    log                     = /var/log/sphinxsearch/searchd.log

    # query log file, all search queries are logged here
    # optional, default is empty (do not log queries)
    query_log               = /var/log/sphinxsearch/query.log

    # client read timeout, seconds
    # optional, default is 5
    read_timeout            = 5

    # request timeout, seconds
    # optional, default is 5 minutes
    client_timeout          = 300

    # maximum amount of children to fork (concurrent searches to run)
    # optional, default is 0 (unlimited)
    max_children            = 30

    # client read timeout, seconds
    # optional, default is 5
    read_timeout            = 5

    # request timeout, seconds
    # optional, default is 5 minutes
    client_timeout          = 300

    # maximum amount of children to fork (concurrent searches to run)
    # optional, default is 0 (unlimited)
    max_children            = 30

    # maximum amount of persistent connections from this master to each agent host
    # optional, but necessary if you use agent_persistent. It is reasonable to set the value
    # as max_children, or less on the agent's hosts.
    persistent_connections_limit    = 30

    # PID file, searchd process ID file name
    # mandatory
    pid_file                = /var/run/sphinxsearch/searchd.pid

    # seamless rotate, prevents rotate stalls if precaching huge datasets
    # optional, default is 1
    seamless_rotate         = 1

    # whether to forcibly preopen all indexes on startup
    # optional, default is 1 (preopen everything)
    preopen_indexes         = 1

    # whether to unlink .old index copies on succesful rotation.
    # optional, default is 1 (do unlink)
    unlink_old              = 1

    # attribute updates periodic flush timeout, seconds
    # updates will be automatically dumped to disk this frequently
    # optional, default is 0 (disable periodic flush)
    #
    # attr_flush_period     = 900


    # MVA updates pool size
    # shared between all instances of searchd, disables attr flushes!
    # optional, default size is 1M
    mva_updates_pool        = 1M

    # max allowed network packet size
    # limits both query packets from clients, and responses from agents
    # optional, default size is 8M
    max_packet_size         = 8M

    # max allowed per-query filter count
    # optional, default is 256
    max_filters             = 256

    # max allowed per-filter values count
    # optional, default is 4096
    max_filter_values       = 4096

    # sudo searchd -c sphinx.conf - to start search daemon listening on above port
    # mysql -P 9306 -h 127.0.0.1 - connect to sphinx server daemon
}
```

### Query Builder Documentation

The SphinxQL query builder package for PHP developed and made available by the kind folks at
foolcode is VERY well documented and tested. I strongly recommend you go
through their webpage at :
http://foolcode.github.io/SphinxQL-Query-Builder/

### Misc Usage Tips

Using Laravel 4 model events (http://four.laravel.com/docs/eloquent#model-events),
it is trivial to ensure that your model stays in sync with your index!

Consider the following snippets that can be easily added into
"created", "updated" and "deleted" events for your model:

```php
Blog::created(function($model){
	$qins = SphinxQL::query()->insert()->into('rt_test');
	$qins->set(array('id'=>99, 'title'=>'My Title', 'content'=>'My Content', 'gid'=>444))->execute();
	//more realistically, it will look something like
	//$qins->set($model->toArray())->execute();
});
```
Similarly, Replace and delete can be easily handled like so:

```php
Blog::updated(function($model){
	$qrepl = SphinxQL::query()->replace()->into('rt_test');
	$qrepl->set(array('id'=>99, 'title'=>'My Title', 'content'=>'My Content', 'gid'=>444))->execute();
});
```
```php
Blog::deleted(function($model){
	SphinxQL::query()->delete()->from('rt_test')->where('id',$model->id)->execute();
});
```
A search query can be constructed as:
```php
$q = SphinxQL::query()->select()->from('rt_test')->match('content', 'test');
	       ->execute();
```
The above statement returns an array of hits (if found).

View the generated sql statement:
```php
dd($q->compile()->getCompiled());
```

Please refer to the documentation
(http://foolcode.github.io/SphinxQL-Query-Builder/) for all available options.

Get the Meta info:
```php
SphinxQL::query()->meta();
```
It is also possible to run a raw sql query against the server like so:
```php
$q = SphinxQL::raw('select * from rt_test');
```
You can pass in any valid SphinxQL statement as a parameter to the raw() function.

### Integration with Eloquent

This package makes it really easy to integrate the results of a search with
database rows. Remember that a sphinx query only returns a hit array containing ID's.
It is upto the application to issue queries against the database to retrieve the actual table rows.

```php
$q = SphinxQL::query()->select()
			->from('rt_test')
			->match('content', 'test')
	       	->execute();

dd(Sphinx::with($q)->get('Blog'));
```
The first statement runs the query against the sphinx server and returns an array.

The "with()" function takes the "hit" array returned by the Sphinx search engine and chains it to the "get".
The "get()" function has the following signature:
```php
public function get($name=null, $key='id')
```
where $name is either:

1. null - The function simply returns an array of ID's

2. An eloquent model - The function returns an EloquentCollection containing table rows matching the ids

3. A string representing a table name - The function returns an array of rows from the table specified (using DB::table('name'))

The $key parameter can be used to change the primary key column name (defaults to 'id')