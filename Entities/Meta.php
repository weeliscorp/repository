<?php

namespace Weelis\Repository\Entities;

use Jenssegers\Mongodb\Eloquent\Model;
use Weelis\Repository\Eloquent\HybridRelations;
use Weelis\Repository\Traits\Morphable;

class Meta extends Model
{
    // Fixme: temporary fix morphto mongpdb relation
    use Morphable, HybridRelations;
    
    protected $connection = 'mongodb';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'meta_data';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //protected $fillable = ['name', 'value', 'type'];

    protected $guarded = ['id'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function metable()
    {
        return $this->morphTo();
    }
}
