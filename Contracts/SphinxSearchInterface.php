<?php
namespace Weelis\Repository\Contracts;

use Illuminate\Contracts\Cache\Repository as CacheRepository;

/**
 * Interface CacheableInterface
 * @package Weelis\Repository\Contracts
 */
interface SphinxSearchInterface
{
    /**
     * Set sphinxsearch realtime index
     * @return mixed
     */
    public function sphinxIndex();

    /**
     * Build sphinxsearch
     * @return mixed
     */
    public function sphinxMake();

    /**
     * Add sphinx condition
     * @param $condition
     * @param $field
     * @param $value
     * @return mixed
     */
    public function sphinxCondition($condition, $field, $value, $operator);

	/**
	 * Search by multiple condition excepted match
	 *
	 * @param        $where
	 * @param string $operation
	 *
	 * @return $this
	 */
	public function sphinxConditions($where, $operation = 'and');

    /**
     * Allow create custom operation sphinxql search
     * @param $operation
     * @return $this
     */
    public function sphinxOperationSearch($operation);

    /**
     * Add custom field operation in sphinxsearch
     * @param $operation
     * @param $alias
     * @return $this
     */
    public function sphinxCustomField($operation, $alias);

    /**
     * Search distance radius
     * @param $field_long
     * @param $field_lat
     * @param $long
     * @param $lat
     * @param $distance meter
     * @return $this
     */
    public function sphinxGeoDistanceSearch($field_long, $field_lat, $long, $lat, $distance);

	/**
	 * Search geo point in array distance
	 *
	 * @param      $array_field
	 * @param      $field_long
	 * @param      $field_lat
	 * @param      $field_distance
	 * @param      $long
	 * @param      $lat
	 * @param bool $empty_true
	 *
	 * @return $this
	 */
    public function sphinxArrayGeoDistanceSearch($array_field, $field_long, $field_lat, $field_distance, $long, $lat, $empty_true = true);

	/**
	 * Search distance inside json field
	 *
	 * @param        $json_field
	 * @param        $field_long
	 * @param        $field_lat
	 * @param array  $range
	 *
	 * @param string $operation
	 *
	 * @return $this
	 */
	public function sphinxJsonGeoDistanceSearch($json_field, $field_long, $field_lat, array $range, $operation = 'or');

	/**
	 * Search inside json column
	 *
	 * @param        $json_field
	 * @param        $where
	 * @param string $comparison | any, all
	 * @param string $operation
	 *
	 * @return $this
	 */
	public function sphinxJsonWhere($json_field, $where, $comparison = 'any', $operation = 'and');

    /**
     * Sphinx order by column
     * @param $column
     * @param null $direction
     * @return mixed
     */
    public function sphinxOrderBy($column, $direction = null);

    /**
     * Sphinx group by column
     * @param $column
     * @return mixed
     */
    public function sphinxGroupBy($column);

    /**
     * Sphinx within group order by column
     * @param $column
     * @param null $direction
     * @return mixed
     */
    public function sphinxWithinGroupOrderBy($column, $direction = null);

    /**
     * Set a SphinxQL option such as max_matches or reverse_scan for the query.
     * @param $name
     * @param $value
     * @return mixed
     */
    public function sphinxOption($name, $value);

    /**
     * Set the offset. Since SphinxQL doesn't support the OFFSET keyword, LIMIT has been set at an extremely high number.
     * @param $limit
     * @param int $offset
     * @return mixed
     */
    public function sphinxLimit($limit, $offset=0);

    /**
     * Support Multi-Query
     * @return mixed
     */
    public function sphinxEnqueue();

    /**
     * Paginate sphinx record return model
     *
     * @param null  $limit
     * @param array $columns
     * @param       $method
     *
     * @return mixed
     */
    public function sphinxPaginate($limit = null, $columns = ['*'], $method = LengthAwarePaginator::class);

	/**
	 * Aggregate custom fields sphinxsearch
	 *
	 * @param bool $reset
	 *
	 * @return mixed
	 */
	public function sphinxAggregate($reset = true);

	/**
	 * Set return aggregate custom fields sphinxsearch
	 *
	 * @param bool $boolean
	 */
	public function sphinxSetAggregate($boolean = false);

	/**
	 * Reset sphinx variable
	 *
	 * @return $this
	 */
	public function sphinxReset();

    /**
     * Insert values into sphinxsearch
     *
     * @param array $array
     *
     * @return mixed
     * @throws \Foolz\SphinxQL\Exception\DatabaseException
     */
    public function sphinxInsert($array = []);

    /**
     * Update values into sphinxsearch
     * Replace values if existed
     *
     * @param array $array
     *
     * @return mixed
     * @throws \Foolz\SphinxQL\Exception\DatabaseException
     */
    public function sphinxUpdate($array = []);

    /**
     * Delete record with id from sphinxsearch
     *
     * @param $id
     *
     * @return mixed
     * @throws \Foolz\SphinxQL\Exception\DatabaseException
     */
    public function sphinxDelete($id);

    /**
     * Truncate sphinx index
     *
     * @return mixed
     */
    public function sphinxTruncate();

    /**
     * Build excerpts
     *
     * @param        $messages
     * @param        $q
     *
     * @param string $start_tag
     * @param string $end_tag
     * @param int    $limit
     *
     * @return \Foolz\SphinxQL\Drivers\ResultSetInterface
     */
    public function sphinxBuildExcerpt($messages, $q, $start_tag = '<b>', $end_tag = '</b>', $limit = 5);
}