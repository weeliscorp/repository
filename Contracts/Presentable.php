<?php
namespace Weelis\Repository\Contracts;

/**
 * Interface Presentable
 * @package Weelis\Repository\Contracts
 */
interface Presentable
{
    /**
     * @param PresenterInterface $presenter
     * @return mixed
     */
    public function setPresenter(PresenterInterface $presenter);

    /**
     * @return mixed
     */
    public function presenter();
}