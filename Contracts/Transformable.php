<?php
namespace Weelis\Repository\Contracts;

/**
 * Interface Transformable
 * @package Weelis\Repository\Contracts
 */
interface Transformable
{
    /**
     * @return array
     */
    public function transform();
}