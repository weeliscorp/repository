<?php
namespace Weelis\Repository\Contracts;

/**
 * Interface RepositoryInterface
 * @package Weelis\Repository\Contracts
 */
interface RepositoryInterface
{

    /**
     * Get current model
     * @return mixed
     */
    public function getModel();

    /**
     * Get query builder
     * @return mixed
     */
    public function getQuery();

    /**
     * Reset repository
     * @return $this
     */
    public function reset();

    /**
     * @throws RepositoryException
     */
    public function resetModel();

    /**
     * Retrieve data array for populate field select
     * 
     * @param string $column
     * @param string|null $key
     *
     * @return \Illuminate\Support\Collection|array
     */
    public function lists($column, $key = null);

    /**
     * Count model
     * 
     * @return number
     */
    public function count();

    /**
     * Retrieve all data of repository
     *
     * @param array $columns
     *
     * @return mixed
     */
    public function all($columns = ['*']);

	/**
	 * Get data of repository
	 *
	 * @param array $columns
	 *
	 * @return mixed
	 */
	public function get($columns = ['*']);

    /**
     * Get latest result
     * @param array $columns
     *
     * @return mixed
     */
    public function latest($columns = ['*']);

    /**
     * Retrieve first data of repository
     *
     * @param array $columns
     *
     * @return mixed
     */
    public function first($columns = ['*']);

    /**
     * Retrieve all data of repository, paginated
     *
     * @param null  $limit
     * @param array $columns
     *
     * @return mixed
     */
    public function paginate($limit = null, $columns = ['*']);

    /**
     * Retrieve all data of repository, simple paginated
     *
     * @param null  $limit
     * @param array $columns
     *
     * @return mixed
     */
    public function simplePaginate($limit = null, $columns = ['*']);

    /**
     * Find data by id
     *
     * @param       $id
     * @param array $columns
     *
     * @return mixed
     */
    public function find($id, $columns = ['*']);

    /**
     * Find data by field and value
     *
     * @param       $field
     * @param       $value
     * @param array $columns
     *
     * @return mixed
     */
    public function findByField($field, $value, $columns = ['*']);

    /**
     * Find data by multiple fields
     *
     * @param array $where
     * @param array $columns
     *
     * @return mixed
     */
    public function findWhere(array $where, $columns = ['*']);

    /**
     * Find data by multiple values in one field
     *
     * @param       $field
     * @param array $values
     * @param array $columns
     *
     * @return mixed
     */
    public function findWhereIn($field, array $values, $columns = ['*']);

    /**
     * Find data by excluding multiple values in one field
     *
     * @param       $field
     * @param array $values
     * @param array $columns
     *
     * @return mixed
     */
    public function findWhereNotIn($field, array $values, $columns = ['*']);

    /**
     * Save a new entity in repository
     *
     * @param array $attributes
     *
     * @return mixed
     */
    public function create(array $attributes);

    /**
     * Update a entity in repository by id
     *
     * @param array $attributes
     * @param       $id
     *
     * @return mixed
     */
    public function update(array $attributes, $id);

	/**
	 * Update a entity in repository by id
	 *
	 * @throws ValidatorException
	 *
	 * @param array $attributes
	 *
	 * @return mixed
	 */
	public function updateWhere(array $attributes);

    /**
     * Update or Create an entity in repository
     *
     * @throws ValidatorException
     *
     * @param array $attributes
     * @param array $values
     *
     * @return mixed
     */
    public function updateOrCreate(array $attributes, array $values = []);

    /**
     * Delete a entity in repository by id
     *
     * @param $id
     *
     * @return int
     */
    public function delete($id);

    /**
     * Delete multiple entities by given criteria.
     *
     * @param array $where
     *
     * @return int
     */
    public function deleteWhere(array $where);

    /**
     * Check if entity has relation
     *
     * @param string $relation
     *
     * @return $this
     */
    public function has($relation);

    /**
     * Load relation has with closure
     *
     * @param string $relation
     * @param closure $closure
     *
     * @return $this
     */
    function whereHas($relation, $closure);

	/**
	 * Check if entity doesn't have relation
	 *
	 * @param string $relation
	 *
	 * @return $this
	 */
	public function doesntHave($relation);

	/**
	 * Load relation doesn't have with closure
	 *
	 * @param string $relation
	 * @param closure $closure
	 *
	 * @return $this
	 */
	function whereDoesntHave($relation, $closure);

    /**
     * Order collection by a given column
     *
     * @param string $column
     * @param string $direction
     *
     * @return $this
     */
    public function orderBy($column, $direction = 'asc');

    /**
     * Load relations
     *
     * @param $relations
     *
     * @return $this
     */
    public function with($relations);

	/**
	 * Load relations count
	 *
	 * @param $relations
	 *
	 * @return $this
	 */
	public function withCount($relations);

    /**
     * Set hidden fields
     *
     * @param array $fields
     *
     * @return $this
     */
    public function hidden(array $fields);

    /**
     * Set visible fields
     *
     * @param array $fields
     *
     * @return $this
     */
    public function visible(array $fields);

    /**
     * Query Scope
     *
     * @param \Closure $scope
     *
     * @return $this
     */
    public function scopeQuery(\Closure $scope);

	/**
	 * When with closure
	 *
	 * @param $condition
	 * @param $closure
	 * @return $this
	 */
	function when($condition, $closure);

	/**
	 * Set transform results
	 *
	 * @param $transform
	 * @return $this
	 */
	public function setTransform($transform);

    /**
     * Reset Query Scope
     *
     * @return $this
     */
    public function resetScope();

    /**
     * Get Searchable Fields
     *
     * @return array
     */
    public function getFieldsSearchable();

    /**
     * Specify Presenter class name
     *
     * @return string
     */
    public function presenter();

    /**
     * Set Presenter
     *
     * @param $presenter
     *
     * @return mixed
     */
    public function setPresenter($presenter);

    /**
     * Skip Presenter Wrapper
     *
     * @param bool $status
     *
     * @return $this
     */
    public function skipPresenter($status = true);
}
