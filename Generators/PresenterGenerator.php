<?php
	namespace Weelis\Repository\Generators;

	/**
	 * Class PresenterGenerator
	 * @package Weelis\Repository\Generators
	 */

	class PresenterGenerator extends Generator
	{
		/**
		 * Get stub name.
		 *
		 * @var string
		 */
		protected $stub = 'presenter/presenter';

		/**
		 * Get root namespace.
		 *
		 * @return string
		 */
		public function getRootNamespace()
		{
			return parent::getRootNamespace() . parent::getConfigGeneratorClassPath($this->getPathConfigNode());
		}

		/**
		 * Get generator path config node.
		 *
		 * @return string
		 */
		public function getPathConfigNode()
		{
			return 'presenters';
		}

		/**
		 * Get array replacements.
		 *
		 * @return array
		 */
		public function getReplacements()
		{
			$transformerGenerator = new TransformerGenerator([
				'module' => $this->module,
				'name'   => $this->name
			]);
			$transformer = $transformerGenerator->getRootNamespace() . '\\' . $transformerGenerator->getName();
			$transformer = str_replace([
				"\\",
				'/'
			], '\\', $transformer);
			$transformerName = $transformerGenerator->getName();

			return array_merge(parent::getReplacements(), [
				'transformer'     => $transformer,
				'transformername' => $transformerName
			]);
		}

		/**
		 * Get destination path for generated file.
		 *
		 * @return string
		 */
		public function getPath()
		{
			return $this->getBasePath() . '/' . parent::getConfigGeneratorClassPath($this->getPathConfigNode(), true) . '/' . $this->getName() . '.php';
		}

		public function getName()
		{
			return parent::getName() . 'Presenter';
		}
	}
