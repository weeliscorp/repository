<?php
	namespace Weelis\Repository\Generators;

	use Illuminate\Support\Str;

	/**
	 * Class TransformerGenerator
	 * @package Weelis\Repository\Generators
	 */
	class CriteriaGenerator extends Generator
	{
		/**
		 * Get stub name.
		 *
		 * @var string
		 */
		protected $stub = 'criteria/criteria';

		/**
		 * Get root namespace.
		 *
		 * @return string
		 */
		public function getRootNamespace()
		{
			return parent::getRootNamespace() . parent::getConfigGeneratorClassPath($this->getPathConfigNode());
		}

		/**
		 * Get generator path config node.
		 *
		 * @return string
		 */
		public function getPathConfigNode()
		{
			return 'criteria';
		}

		/**
		 * Get destination path for generated file.
		 *
		 * @return string
		 */
		public function getPath()
		{
			return $this->getBasePath() . '/' . parent::getConfigGeneratorClassPath($this->getPathConfigNode(), true) . '/' . $this->getName() . '.php';
		}

		public function getName()
		{
			return parent::getName().'Criteria';
		}

		/**
		 * Get base path of destination file.
		 *
		 * @return string
		 */
		public function getBasePath()
		{
			$module_path = $this->getModulePath();
			if (isset($module_path)) {
				return $module_path;
			}

			return config('repository.generator.basePath', app_path());
		}

		/**
		 * Get array replacements.
		 *
		 * @return array
		 */
		public function getReplacements()
		{
			$interfaceGen = new RepositoryInterfaceGenerator([
				'module' => $this->module,
				'name'   => $this->name
			]);

			$interface = $interfaceGen->getRootNamespace() . '\\' . $interfaceGen->getName();
			$interface = str_replace([
				"\\",
				'/'
			], '\\', $interface);
			$interfaceName = $interfaceGen->getName();

			return array_merge(parent::getReplacements(), [
				'interface'     => $interface,
				'interfacename' => $interfaceName
			]);
		}
	}
