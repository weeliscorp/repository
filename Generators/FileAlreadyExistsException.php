<?php
namespace Weelis\Repository\Generators;

use Exception;

/**
 * Class FileAlreadyExistsException
 * @package Weelis\Repository\Generators
 */
class FileAlreadyExistsException extends Exception
{
}