<?php
namespace Weelis\Repository\Generators\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Weelis\Repository\Generators\FileAlreadyExistsException;
use Weelis\Repository\Generators\ManagerGenerator;
use Weelis\Repository\Generators\MigrationGenerator;
use Weelis\Repository\Generators\ModelGenerator;
use Weelis\Repository\Generators\RepositoryEloquentGenerator;
use Weelis\Repository\Generators\RepositoryInterfaceGenerator;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class ManagerCommand extends Command
{

    /**
     * The name of command.
     *
     * @var string
     */
    protected $name = 'make:manager';

    /**
     * The description of command.
     *
     * @var string
     */
    protected $description = 'Create a new manager.';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Manager';

    /**
     * @var Collection
     */
    protected $generators = null;


    /**
     * Execute the command.
     *
     * @return void
     */
    public function fire()
    {
        try {
            (new ManagerGenerator([
                'module'  => $this->argument('module'),
                'name' => $this->argument('name'),
                'action' => $this->option('action'),
            ]))->run();
            $this->info("Manager created successfully.");
        } catch (FileAlreadyExistsException $e) {
            $this->error($this->type . ' already exists!');

            return false;
        }
    }


    /**
     * The array of command arguments.
     *
     * @return array
     */
    public function getArguments()
    {
        return [
            [
                'name',
                InputArgument::REQUIRED,
                'The name of class being generated.',
                null
            ],
            [
                'module',
                InputArgument::OPTIONAL,
                'The module of class being generated.',
                null
            ],
        ];
    }

    /**
     * The array of command options.
     *
     * @return array
     */
    public function getOptions()
    {
        return [
            [
                'action',
                'f',
                InputOption::VALUE_REQUIRED,
                'Action already exists.',
                null
            ]
        ];
    }

}
