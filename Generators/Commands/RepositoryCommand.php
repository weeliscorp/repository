<?php
	namespace Weelis\Repository\Generators\Commands;

	use Illuminate\Console\Command;
	use Illuminate\Support\Collection;
	use Weelis\Repository\Generators\BindingsGenerator;
	use Weelis\Repository\Generators\FacadeGenerator;
	use Weelis\Repository\Generators\FileAlreadyExistsException;
	use Weelis\Repository\Generators\MigrationGenerator;
	use Weelis\Repository\Generators\ModelGenerator;
	use Weelis\Repository\Generators\RepositoryEloquentGenerator;
	use Weelis\Repository\Generators\RepositoryInterfaceGenerator;
	use Symfony\Component\Console\Input\InputArgument;
	use Symfony\Component\Console\Input\InputOption;
	use Module;

	class RepositoryCommand extends Command
	{

		/**
		 * The name of command.
		 *
		 * @var string
		 */
		protected $name = 'make:repository';

		/**
		 * The description of command.
		 *
		 * @var string
		 */
		protected $description = 'Create a new repository.';

		/**
		 * The type of class being generated.
		 *
		 * @var string
		 */
		protected $type = 'Repository';

		/**
		 * @var Collection
		 */
		protected $generators = null;


		/**
		 * Execute the command.
		 *
		 * @return void
		 */
		public function fire()
		{
			$this->generators = new Collection();

			$this->generators->push(new MigrationGenerator([
				'module' => $this->argument('module'),
				'name'   => 'create_' . snake_case(str_plural($this->argument('name'))) . '_table',
				'fields' => $this->option('fillable'),
				'force'  => $this->option('force'),
			]));

			$modelGenerator = new ModelGenerator([
				'module'   => $this->argument('module'),
				'name'     => $this->argument('name'),
				'fillable' => $this->option('fillable'),
				'force'    => $this->option('force')
			]);

			$this->generators->push($modelGenerator);

			$this->generators->push(new RepositoryInterfaceGenerator([
				'module' => $this->argument('module'),
				'name'   => $this->argument('name'),
				'force'  => $this->option('force'),
			]));

			$this->generators->push(new RepositoryEloquentGenerator([
				'module'    => $this->argument('module'),
				'name'      => $this->argument('name'),
				'rules'     => $this->option('rules'),
				'validator' => $this->option('validator'),
				'force'     => $this->option('force')
			]));

			$this->generators->push(new FacadeGenerator([
				'module'    => $this->argument('module'),
				'name'      => $this->argument('name'),
				'force'  => $this->option('force')
			]));

			$bindingGenerator = new BindingsGenerator([
				'module'    => $this->argument('module'),
				'name'      => $this->argument('name'),
				'force'  => $this->option('force')
			]);
			
			try {
				foreach ($this->generators as $generator) {
					$generator->run();
				}
				$this->info($this->type .  " created successfully.");

				$this->warn('Place this code into: ' . $bindingGenerator->getPath());
				$this->info('======================================================');
				$this->info($bindingGenerator->run());
				$this->info('======================================================');

			} catch (FileAlreadyExistsException $e) {
				$this->error($this->type . ' already exists!');

				return false;
			}
		}


		/**
		 * The array of command arguments.
		 *
		 * @return array
		 */
		public function getArguments()
		{
			return [
				[
					'name',
					InputArgument::REQUIRED,
					'The name of class being generated.',
					null
				],
				[
					'module',
					InputArgument::OPTIONAL,
					'The module of class being generated.',
					null
				],
			];
		}


		/**
		 * The array of command options.
		 *
		 * @return array
		 */
		public function getOptions()
		{
			return [
				[
					'fillable',
					null,
					InputOption::VALUE_OPTIONAL,
					'The fillable attributes.',
					null
				],
				[
					'rules',
					null,
					InputOption::VALUE_OPTIONAL,
					'The rules of validation attributes.',
					null
				],
				[
					'validator',
					null,
					InputOption::VALUE_OPTIONAL,
					'Adds validator reference to the repository.',
					null
				],
				[
					'force',
					'f',
					InputOption::VALUE_NONE,
					'Force the creation if file already exists.',
					null
				]
			];
		}
	}
