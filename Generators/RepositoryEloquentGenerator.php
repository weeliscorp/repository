<?php
	namespace Weelis\Repository\Generators;

	use Illuminate\Support\Str;
	use Weelis\Repository\Generators\Migrations\SchemaParser;

	/**
	 * Class RepositoryEloquentGenerator
	 * @package Weelis\Repository\Generators
	 */
	class RepositoryEloquentGenerator extends Generator
	{

		/**
		 * Get stub name.
		 *
		 * @var string
		 */
		protected $stub = 'repository/eloquent';

		/**
		 * Get root namespace.
		 *
		 * @return string
		 */
		public function getRootNamespace()
		{
			return parent::getRootNamespace() . parent::getConfigGeneratorClassPath($this->getPathConfigNode());
		}

		/**
		 * Get generator path config node.
		 *
		 * @return string
		 */
		public function getPathConfigNode()
		{
			return 'repositories';
		}

		/**
		 * Get destination path for generated file.
		 *
		 * @return string
		 */
		public function getPath()
		{
			return $this->getBasePath() . '/' . parent::getConfigGeneratorClassPath($this->getPathConfigNode(), true) . '/' . $this->getName() . '.php';
		}

		public function getName()
		{
			return parent::getName() . 'RepositoryEloquent';
		}

		/**
		 * Get array replacements.
		 *
		 * @return array
		 */
		public function getReplacements()
		{
			$interfaceGen = new RepositoryInterfaceGenerator([
				'module' => $this->module,
				'name'   => $this->name
			]);

			$repository = $interfaceGen->getRootNamespace() . '\\' . $interfaceGen->getName();
			$repository = str_replace([
				"\\",
				'/'
			], '\\', $repository);
			$repositoryName = $interfaceGen->getName();

			$modelGenerator = new ModelGenerator([
				'module' => $this->module,
				'name'   => $this->name
			]);
			$model = $modelGenerator->getRootNamespace() . '\\' . $modelGenerator->getName();
			$model = str_replace([
				"\\",
				'/'
			], '\\', $model);
			$modelName = $modelGenerator->getName();

			return array_merge(parent::getReplacements(), [
				'fillable'       => $this->getFillable(),
				'use_validator'  => $this->getValidatorUse(),
				'validator'      => $this->getValidatorMethod(),
				'repository'     => $repository,
				'repositoryname' => $repositoryName,
				'model'          => $model,
				'modelName'      => $modelName
			]);
		}

		/**
		 * Get the fillable attributes.
		 *
		 * @return string
		 */
		public function getFillable()
		{
			if (!$this->fillable) {
				return '[]';
			}
			$results = '[' . PHP_EOL;

			foreach ($this->getSchemaParser()->toArray() as $column => $value) {
				$results .= "\t\t'{$column}'," . PHP_EOL;
			}

			return $results . "\t" . ']';
		}

		/**
		 * Get schema parser.
		 *
		 * @return SchemaParser
		 */
		public function getSchemaParser()
		{
			return new SchemaParser($this->fillable);
		}

		public function getValidatorUse()
		{
			$validator = $this->getValidator();

			return "use {$validator};";
		}


		public function getValidator()
		{
			$validatorGenerator = new ValidatorGenerator([
				'module' => $this->module,
				'name'   => $this->name,
				'rules'  => $this->rules,
				'force'  => $this->force,
			]);

			$validator = $validatorGenerator->getRootNamespace() . '\\' . $validatorGenerator->getName();

			return str_replace([
				"\\",
				'/'
			], '\\', $validator) . 'Validator';

		}


		public function getValidatorMethod()
		{
			if ($this->validator != 'yes') {
				return '';
			}

			$class = $this->getClass();

			return '/**' . PHP_EOL . '    * Specify Validator class name' . PHP_EOL . '    *' . PHP_EOL . '    * @return mixed' . PHP_EOL . '    */' . PHP_EOL . '    public function validator()' . PHP_EOL . '    {' . PHP_EOL . PHP_EOL . '        return ' . $class . 'Validator::class;' . PHP_EOL . '    }' . PHP_EOL;

		}
	}
