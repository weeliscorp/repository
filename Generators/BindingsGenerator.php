<?php
namespace Weelis\Repository\Generators;
use Illuminate\Support\Str;

/**
 * Class BindingsGenerator
 * @package Weelis\Repository\Generators
 */
class BindingsGenerator extends Generator
{

    /**
     * The placeholder for repository bindings
     *
     * @var string
     */
    public $bindPlaceholder = "//:binding-placeholder";
    /**
     * Get stub name.
     *
     * @var string
     */
    protected $stub = 'bindings/bindings';

    public function run()
    {
        // Add entity repository binding to the repository service provider
        return $this->getStub();
//        $provider = \File::get($this->getPath());
//        \File::put($this->getPath(), str_replace($this->bindPlaceholder, '        ' . $this->bindPlaceholder . PHP_EOL . $this->getStub(), $provider));
    }

    /**
     * Get destination path for generated file.
     *
     * @return string
     */
    public function getPath()
    {
        return $this->getBasePath() . '/Providers/' . parent::getConfigGeneratorClassPath($this->getPathConfigNode(), true) . '.php';
    }

    /**
     * Get generator path config node.
     *
     * @return string
     */
    public function getPathConfigNode()
    {
        return 'provider';
    }

    /**
     * Gets eloquent repository full class name
     *
     * @return string
     */
    public function getEloquentRepository()
    {
        $repositoryGenerator = new RepositoryEloquentGenerator([
            'module' => $this->module,
            'name' => $this->name,
        ]);

        $repository = $repositoryGenerator->getRootNamespace() . '\\' . $repositoryGenerator->getName();

        return str_replace([
            "\\",
            '/'
        ], '\\', $repository);
    }

    /**
     * Get root namespace.
     *
     * @return string
     */
    public function getRootNamespace()
    {
        return parent::getRootNamespace() . parent::getConfigGeneratorClassPath($this->getPathConfigNode());
    }

    /**
     * Get array replacements.
     *
     * @return array
     */
    public function getReplacements()
    {
        $repositoryGenerator = new RepositoryInterfaceGenerator([
            'module' => $this->module,
            'name'   => $this->name,
        ]);

        $repository = $repositoryGenerator->getRootNamespace() . '\\' . $repositoryGenerator->getName();

        $repository = str_replace([
            "\\",
            '/'
        ], '\\', $repository);
        $repositorySnake = Str::snake($repositoryGenerator->getName());

        return array_merge(parent::getReplacements(), [
            'repository' => '\\'.$repository,
            'repositorysnake' => $repositorySnake,
            'eloquent' => '\\'.$this->getEloquentRepository()
        ]);
    }
}
