<?php
	/*
	|--------------------------------------------------------------------------
	| Weelis Repository Config
	|--------------------------------------------------------------------------
	|
	|
	*/
	return [
		/*
		|--------------------------------------------------------------------------
		| Repository Spinxsearch connection
		|--------------------------------------------------------------------------
		 */
		'sphinxsearch' => [
			'host'        => env('SPHINX_HOST', 'localhost'),
			'port'        => env('SPHINX_PORT', 9306),
			'max_matches' => 1000000,
			'index_empty' => 'weelisempty'
		],
		/*
		|--------------------------------------------------------------------------
		| Repository Pagination Limit Default
		|--------------------------------------------------------------------------
		|
		*/
		'pagination'   => [
			'limit' => 15
		],

		/*
		|--------------------------------------------------------------------------
		| Fractal Presenter Config
		|--------------------------------------------------------------------------
		|

		Available serializers:
		ArraySerializer
		DataArraySerializer
		JsonApiSerializer

		*/
		'fractal'      => [
			'params'     => [
				'include' => 'include'
			],
			'serializer' => League\Fractal\Serializer\DataArraySerializer::class
		],

		/*
		|--------------------------------------------------------------------------
		| Cache Config
		|--------------------------------------------------------------------------
		|
		*/
		'cache'        => [
			/*
			 |--------------------------------------------------------------------------
			 | Cache Status
			 |--------------------------------------------------------------------------
			 |
			 | Enable or disable cache
			 |
			 */
			'enabled'    => true,

			/*
			 |--------------------------------------------------------------------------
			 | Cache Minutes
			 |--------------------------------------------------------------------------
			 |
			 | Time of expiration cache
			 |
			 */
			'minutes'    => 30,

			/*
			 |--------------------------------------------------------------------------
			 | Cache Repository
			 |--------------------------------------------------------------------------
			 |
			 | Instance of Illuminate\Contracts\Cache\Repository
			 |
			 */
			'repository' => 'cache',

			/*
			  |--------------------------------------------------------------------------
			  | Cache Clean Listener
			  |--------------------------------------------------------------------------
			  |
			  |
			  |
			  */
			'clean'      => [

				/*
				  |--------------------------------------------------------------------------
				  | Enable clear cache on repository changes
				  |--------------------------------------------------------------------------
				  |
				  */
				'enabled' => true,

				/*
				  |--------------------------------------------------------------------------
				  | Actions in Repository
				  |--------------------------------------------------------------------------
				  |
				  | create : Clear Cache on create Entry in repository
				  | update : Clear Cache on update Entry in repository
				  | delete : Clear Cache on delete Entry in repository
				  |
				  */
				'on'      => [
					'create' => true,
					'update' => true,
					'delete' => true,
				]
			],

			'params'  => [
				/*
				|--------------------------------------------------------------------------
				| Skip Cache Params
				|--------------------------------------------------------------------------
				|
				|
				| Ex: http://weelis.com/?search=lorem&skipCache=true
				|
				*/
				'skipCache' => 'skipCache'
			],

			/*
		   |--------------------------------------------------------------------------
		   | Methods Allowed
		   |--------------------------------------------------------------------------
		   |
		   | methods cacheable : all, paginate, find, findByField, findWhere, getByCriteria
		   |
		   | Ex:
		   |
		   | 'only'  =>['all','paginate'],
		   |
		   | or
		   |
		   | 'except'  =>['find'],
		   */
			'allowed' => [
				'only'   => null,
				'except' => null
			]
		],

		/*
		|--------------------------------------------------------------------------
		| Criteria Config
		|--------------------------------------------------------------------------
		|
		| Settings of request parameters names that will be used by Criteria
		|
		*/
		'criteria'     => [
			/*
			|--------------------------------------------------------------------------
			| Accepted Conditions
			|--------------------------------------------------------------------------
			|
			| Conditions accepted in consultations where the Criteria
			|
			| Ex:
			|
			| 'acceptedConditions'=>['=','like']
			|
			| $query->where('foo','=','bar')
			| $query->where('foo','like','bar')
			|
			*/
			'acceptedConditions' => [
				'=', 'like'
			],
			/*
			|--------------------------------------------------------------------------
			| Request Params
			|--------------------------------------------------------------------------
			|
			| Request parameters that will be used to filter the query in the repository
			|
			| Params :
			|
			| - search : Searched value
			|   Ex: http://weelis.com/?search=lorem
			|
			| - searchFields : Fields in which research should be carried out
			|   Ex:
			|    http://weelis.com/?search=lorem&searchFields=name;email
			|    http://weelis.com/?search=lorem&searchFields=name:like;email
			|    http://weelis.com/?search=lorem&searchFields=name:like
			|
			| - filter : Fields that must be returned to the response object
			|   Ex:
			|   http://weelis.com/?search=lorem&filter=id,name
			|
			| - orderBy : Order By
			|   Ex:
			|   http://weelis.com/?search=lorem&orderBy=id
			|
			| - sortedBy : Sort
			|   Ex:
			|   http://weelis.com/?search=lorem&orderBy=id&sortedBy=asc
			|   http://weelis.com/?search=lorem&orderBy=id&sortedBy=desc
			|
			*/
			'params'             => [
				'search'       => 'search',
				'searchFields' => 'searchFields',
				'filter'       => 'filter',
				'orderBy'      => 'orderBy',
				'sortedBy'     => 'sortedBy',
				'with'         => 'with'
			]
		],
		/*
		|--------------------------------------------------------------------------
		| Generator Config
		| Remove basePath to prevent Docker cached path
		|--------------------------------------------------------------------------
		|
		*/
		'generator'    => [
			'basePath'          => app_path(), 
			'rootNamespace'     => 'App\\',
			'paths'             => [
				'migrations'   => 'Database/Migrations',
				'models'       => 'Entities',
				'repositories' => 'Repositories',
				'interfaces'   => 'Contracts',
				'transformers' => 'Transformers',
				'presenters'   => 'Presenters',
				'criteria'     => 'Criteria',
				'facade'       => 'Facades',
				'validators'   => 'Validators',
				'controllers'  => 'Http/Controllers',
				'provider'     => 'RepositoryServiceProvider',
				'managers'     => 'Services',
			],
			'stubsOverridePath' => 'Generators'
		],

		/*
		|--------------------------------------------------------------------------
		| FakeId connection settings
		|--------------------------------------------------------------------------
		|
		| Since FakeId depends on jenssegers/optimus, we need three values:
		| - A large prime number lower than 2147483647
		| - The inverse prime so that (PRIME * INVERSE) & MAXID == 1
		| - A large random integer lower than 2147483647
		|
		*/

		'prime'   => env('FAKEID_PRIME', 961748927),
		'inverse' => env('FAKEID_INVERSE', 1430310975),
		'random'  => env('FAKEID_RANDOM', 620464665),


	];