## 1.0.7 (Dec 04, 2017)
 - Add sphinxGeoOrderBy && delete where

## 1.0.6 (May 20, 2017)
 - Add get function

## 1.0.5 (May 15, 2017)
 - Remove module
 - Support transform native eloquent, when

## 1.0.3 (November 11, 2016)
 - Support withCount, doesntHave & whereDoesntHave in repository
 - Upgrade core to support laravel 5.3

## 1.0.2 (November 3, 2016)
 - Clean meta trash when create meta data

## 1.0.1 (September 21, 2016)
Set path phantomjs
Fix sphinxsearch wrapper
Fix remove metadata
Add SluggableEnable

Usage:
 - Include `Weelis\Repository\Traits\SluggableEnable` into repository
 - `ModelRepo::findbyString($string, $slug_field, $columns)` make slug then search string in file
 - `ModelRepo::findbySlug($string, $columns)` search by slug-string

## 1.0 (September 14, 2016)
First initial version 1.0

Features:

  - Repository mysql
  - Repository mongodb
  - Repository sphinxsearch
  - Generator support command