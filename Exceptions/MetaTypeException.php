<?php
namespace Weelis\Repository\Exceptions;

use \Exception;

/**
 * Class RepositoryException
 * @package Weelis\Repository\Exceptions
 */
class MetaTypeException extends Exception
{

}