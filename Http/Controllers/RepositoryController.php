<?php namespace Weelis\Repository\Http\Controllers;

use Nwidart\Modules\Routing\Controller;

class RepositoryController extends Controller {
	
	public function index()
	{
		return view('Repository::index');
	}
	
}