<?php

Route::group(['prefix' => 'repository', 'namespace' => 'Weelis\Repository\Http\Controllers'], function()
{
	Route::get('/', 'RepositoryController@index');
});