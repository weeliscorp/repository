<?php
namespace Weelis\Repository\Providers;

use Illuminate\Support\ServiceProvider;

class EventServiceProvider extends ServiceProvider {

    /**
     * The event handler mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'Weelis\Repository\Events\RepositoryEntityCreated' => [
            'Weelis\Repository\Listeners\CleanCacheRepository'
        ],
        'Weelis\Repository\Events\RepositoryEntityUpdated' => [
            'Weelis\Repository\Listeners\CleanCacheRepository'
        ],
        'Weelis\Repository\Events\RepositoryEntityDeleted' => [
            'Weelis\Repository\Listeners\CleanCacheRepository'
        ]
    ];

    /**
     * Register the application's event listeners.
     *
     * @return void
     */
    public function boot()
    {
        $events = app('events');

        foreach ($this->listen as $event => $listeners) {
            foreach ($listeners as $listener) {
                $events->listen($event, $listener);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function register()
    {
        //
    }

    /**
     * Get the events and handlers.
     *
     * @return array
     */
    public function listens()
    {
        return $this->listen;
    }
}
