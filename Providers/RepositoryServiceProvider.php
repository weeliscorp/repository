<?php namespace Weelis\Repository\Providers;

use Foolz\SphinxQL\Drivers\Pdo\Connection;
use Illuminate\Support\ServiceProvider;
use Weelis\Repository\Console\FakeIdSetupCommand;
use Weelis\Repository\Helpers\Sphinxql;
use Weelis\Repository\Helpers\SphinxQLCustom;
use Weelis\Repository\Repositories\Metas;

class RepositoryServiceProvider extends ServiceProvider
{

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = true;

	/**
	 * Boot the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->registerTranslations();
		$this->registerConfig();
	}

	/**
	 * Register translations.
	 *
	 * @return void
	 */
	public function registerTranslations()
	{
		$langPath = base_path('Resources/lang');

		if (is_dir($langPath)) {
			$this->loadTranslationsFrom($langPath, 'statistic');
		} else {
			$this->loadTranslationsFrom(__DIR__ . '/../Resources/lang', 'statistic');
		}
	}

	/**
	 * Register config.
	 *
	 * @return void
	 */
	protected function registerConfig()
	{
		$this->publishes([
			__DIR__ . '/../Config/config.php' => config_path('repository.php'),
		]);
		$this->mergeConfigFrom(
			__DIR__ . '/../Config/config.php', 'repository'
		);
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->commands('Weelis\Repository\Generators\Commands\RepositoryCommand');
		$this->commands('Weelis\Repository\Generators\Commands\CriteriaCommand');
		$this->commands('Weelis\Repository\Generators\Commands\TransformerCommand');
		$this->commands('Weelis\Repository\Generators\Commands\PresenterCommand');
		$this->commands('Weelis\Repository\Generators\Commands\EntityCommand');
		$this->commands('Weelis\Repository\Generators\Commands\ManagerCommand');
		$this->commands('Weelis\Repository\Generators\Commands\ValidatorCommand');
		$this->commands('Weelis\Repository\Generators\Commands\ControllerCommand');
		$this->commands('Weelis\Repository\Generators\Commands\BindingsCommand');
		// Register fakeid setup command.
		$this->app->singleton('fakeid.command.setup', function ($app) {
			return new FakeIdSetupCommand();
		});
		$this->commands('fakeid.command.setup');

		$this->app->bind('meta_repository', Metas::class);

		$this->app->singleton('sphinxql', function ($app) {
			$connection = new Connection();
			$connection->setParams($app['config']->get('repository')['sphinxsearch']);

			return new Sphinxql(new SphinxQLCustom($connection));
		});
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return ['meta_repository', 'sphinxql', 'phantomjs', 'fakeid'];
	}

}
