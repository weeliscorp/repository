<?php

namespace Weelis\Repository\Traits;

use Weelis\Repository\Eloquent\HybridRelations;
use Weelis\Repository\Entities\Meta;
use DB;
use TrackerProfileRepo;

trait MetaEloquentTrait
{
    use HybridRelations;

    public function meta()
    {
        return $this->morphOne(Meta::class, 'metable');
    }

    public function loadMetaAttr()
    {
        // Load one if not exist
        $meta = $this->meta;
        if(isset($meta))
        {
            foreach ($meta->getAttributes() as $name => $value) {
                $meta_type = $meta->{$name.'_type'};
                if(isset($meta_type)) {
                    // Auto cast type
                    $value = TrackerProfileRepo::metaSetType($meta->$name, $meta_type);
                    $this->setAttribute($name, $value);
                }
            }
        }
    }

    // this is a recommended way to declare event handlers
    public static function bootMetaEloquentTrait() {
        static::deleting(function($model) { // before delete() method call this
            // Cleaning all meta data
            $model->meta()->delete();
        });
    }
}