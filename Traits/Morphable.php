<?php

namespace Weelis\Repository\Traits;

use Weelis\Repository\Builder\MorphBuilder;

/**
 * Class CacheableRepository
 * @package Weelis\Repository\Traits
 */
trait Morphable {

    /**
     * Create a new Eloquent query builder for the model.
     *
     * @param  \Illuminate\Database\Query\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function newEloquentBuilder($query)
    {
        return new MorphBuilder($query);
    }
}
