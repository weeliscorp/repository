<?php

namespace Weelis\Repository\Traits;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Weelis\Repository\Exceptions\MetaTypeException;
use Weelis\Repository\Facades\MetaRepo;
use Weelis\Repository\Events\RepositoryEntityUpdated;
use Helpers;

trait MetadataTrait
{
	private $with_meta = false;
	private $type_suffix = '_type';

	public function metaSetType($var, $type)
	{
		$value = $var;
		if ($type != 'array') {
			try {
				settype($value, $type);
			} catch (\Exception $e) {
				$this->changeClass($value, $type);
			}
		}

		return $value;
	}

	/**
	 * Change the class of an object
	 *
	 * @param object $obj
	 * @param        $new_class
	 *
	 * @author toma at smartsemantics dot com
	 * @see    http://www.php.net/manual/en/language.types.type-juggling.php#50791
	 */
	private function changeClass(&$obj, $new_class)
	{
		if (class_exists($new_class, true)) {
			$obj = unserialize($obj);
		}
	}

	public function withMeta()
	{
		$this->with('meta');
		$this->with_meta = true;

		return $this;
	}

	public function createMeta($array, $id)
	{
		$this->applyCriteria();
		$this->applyScope();

		$_skipPresenter = $this->skipPresenter;

		$this->skipPresenter(true);

		$model = $this->withMeta()->find($id);
		if (isset($model)) {
			$old = clone $model;

			// Cleaning trash meta
			$metas = MetaRepo::findWhere([
				'metable_id'   => $model->getKey(),
				'metable_type' => $model->getMorphClass()
			]);
			if (isset($metas)) {
				foreach ($metas as $trash) {
					MetaRepo::delete($trash->id);
				}
			}
			$meta_data = [];

			foreach ($array as $name => $value) {
				// Auto cast type array & object
				$value_type = Helpers::getType($value);
				if ($value_type == 'NULL') {
					continue;
				} elseif (is_object($value)) {
					$value = serialize($value);
				}

				$meta_data = array_merge([
					$name                      => $value,
					$name . $this->type_suffix => $value_type
				], $meta_data);
			}
			$meta = MetaRepo::create($meta_data);
			$model->meta()->save($meta);

			if (!$this->skipEvents) {
				event(new RepositoryEntityUpdated($this, $model, $old));
			}

			$this->skipPresenter($_skipPresenter);
			$this->reset();
		}

		return $this->parserResult($model);
	}

	public function updateMeta($array, $id)
	{
		$this->applyCriteria();
		$this->applyScope();

		$_skipPresenter = $this->skipPresenter;

		$this->skipPresenter(true);

		$model = $this->withMeta()->find($id);
		$old = clone $model;
		$meta = $model->meta;
		if (!isset($meta)) {
			$this->createMeta($array, $id);

			return;
		}
		$meta_data = [];
		foreach ($array as $name => $value) {
			$value_type = Helpers::getType($value);
			if ($value_type == 'NULL') {
				continue;
			}
			// Auto cast type array & object
			if (is_object($value)) {
				$value = serialize($value);
			}
			/*$meta_type = $meta->{$name . $this->type_suffix};
			if (isset($meta_type)) {
				// Validate type before insert
				if ($meta_type != $value_type && !is_numeric($value)) {
					throw new MetaTypeException("This {$name} type {$value_type} not compatible with existing {$meta_type} type");
				}
				$meta_value = [
					$name => $value
				];
				// Pass numeric
				if (is_numeric($value)) {
					$meta_value[ $name . $this->type_suffix ] = $value_type;
				}
				$meta_data = array_merge($meta_value, $meta_data);
			} else {*/ // create
				$meta_data = array_merge([
					$name                      => $value,
					$name . $this->type_suffix => $value_type
				], $meta_data);
			//}
		}
		MetaRepo::update($meta_data, $meta->id);

		if (!$this->skipEvents) {
			event(new RepositoryEntityUpdated($this, $model, $old));
		}

		$this->skipPresenter($_skipPresenter);
		$this->reset();

		return $this->parserResult($model);
	}

	public function unsetMeta($meta_names, $id)
	{
		$this->applyCriteria();
		$this->applyScope();

		$_skipPresenter = $this->skipPresenter;

		$this->skipPresenter(true);

		$model = $this->withMeta()->find($id);
		$old = clone $model;
		if ($meta = $model->meta) {
			foreach ($meta_names as $name) {
				$meta->unset($name);
				$meta->unset($name . $this->type_suffix);
			}
		}

		if (!$this->skipEvents) {
			event(new RepositoryEntityUpdated($this, $model, $old));
		}
		$this->skipPresenter($_skipPresenter);
		$this->reset();

		return $this->parserResult($model);
	}

	public function deleteMeta($id)
	{
		$this->applyCriteria();
		$this->applyScope();

		$_skipPresenter = $this->skipPresenter;

		$this->skipPresenter(true);

		$model = $this->withMeta()->find($id);
		$old = clone $model;

		if ($meta = $model->meta) {
			MetaRepo::delete($meta->id);
		}

		if (!$this->skipEvents) {
			event(new RepositoryEntityUpdated($this, $model, $old));
		}

		$this->skipPresenter($_skipPresenter);
		$this->reset();

		return $this->parserResult($model);
	}

	public function findByMeta(array $attributes)
	{
		// Fixme: fuck that shitty, wrong query
		foreach ($attributes as $name => $value) {
			$this->model = $this->getModel()->whereHas('meta', function ($q) use ($name, $value) {
				$q->where($name, $value);
			});
		}

		return $this;
	}

	public function incrementMeta($name, $id)
	{
		$this->applyCriteria();
		$this->applyScope();

		$_skipPresenter = $this->skipPresenter;

		$this->skipPresenter(true);

		$model = $this->withMeta()->find($id);
		$old = clone $model;
		$model->meta()->increment($name);

		$this->skipPresenter($_skipPresenter);
		$this->reset();
		if (!$this->skipEvents) {
			event(new RepositoryEntityUpdated($this, $model, $old));
		}

		return $this->parserResult($model);
	}

	public function decrementMeta($name, $id)
	{
		$this->applyCriteria();
		$this->applyScope();

		$_skipPresenter = $this->skipPresenter;

		$this->skipPresenter(true);

		$model = $this->withMeta()->find($id);
		$old = clone $model;
		$model->meta()->decrement($name);

		$this->skipPresenter($_skipPresenter);
		$this->reset();
		if (!$this->skipEvents) {
			event(new RepositoryEntityUpdated($this, $model, $old));
		}

		return $this->parserResult($model);
	}

	public function getMeta($id, $with_type = false)
	{
		$this->applyCriteria();
		$this->applyScope();

		$model = $this->getModel()->with('meta')->findOrFail($id);
		$meta = $model->meta;
		if (!isset($meta)) {
			return null;
		}
		$this->reset();

		if ($with_type) {
			$meta_array = [];
			foreach ($meta->getAttributes() as $name => $value) {
				$meta_type = $meta->{$name . $this->type_suffix};
				if (isset($meta_type)) {
					// Add type
					$meta_array[] = [
						'name'  => $name,
						'value' => $this->metaSetType($meta->$name, $meta_type),
						'type'  => $meta_type
					];
				}
			}

			return $meta_array;
		}
		$metaObj = new \stdClass();
		foreach ($meta->getAttributes() as $name => $value) {
			$meta_type = $meta->{$name . $this->type_suffix};
			if (isset($meta_type)) {
				// Auto cast array type
				$value = $meta->$name;
				$metaObj->{$name} = $this->metaSetType($value, $meta_type);
			}
		}

		return $metaObj;
	}

	public function getMetaByName($name, $id)
	{
		$this->applyCriteria();
		$this->applyScope();

		$model = $this->getModel()->with('meta')->findOrFail($id);
		$meta = $model->meta;

		$this->resetModel();
		if ($meta) {
			// Auto cast array type
			return $this->metaSetType($meta->$name, $meta->{$name . $this->type_suffix});
		}

		return null;
	}

	/**
	 * Parse results override for meta
	 *
	 * @param mixed $result
	 *
	 * @return mixed
	 */
	public function parserResult($result)
	{
		$type_suffix = $this->type_suffix;
		$self = $this;
		// Set meta as property
		if ($this->with_meta) {
			if ($result instanceof Collection || $result instanceof LengthAwarePaginator) {
				$result->each(function ($model) use ($self, $type_suffix) {
					$meta = $model->meta;
					if (isset($meta)) {
						foreach ($meta->getAttributes() as $name => $value) {
							$meta_type = $meta->{$name . $type_suffix};
							if (isset($meta_type)) {
								// Auto cast type
								$value = $self->metaSetType($meta->$name, $meta_type);
								$model->setAttribute($name, $value);
							}
						}
					}

					return $model;
				});
			} elseif ($result instanceof Model) {
				$meta = $result->meta;
				if (isset($meta)) {
					foreach ($meta->getAttributes() as $name => $value) {
						$meta_type = $meta->{$name . $type_suffix};
						if (isset($meta_type)) {
							// Auto cast type
							$value = $this->metaSetType($meta->$name, $meta_type);
							$result->setAttribute($name, $value);
						}
					}
				}
			}
		}

		return parent::parserResult($result);
	}
}