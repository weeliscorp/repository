<?php

namespace Weelis\Repository\Traits;

use Foolz\SphinxQL\Helper;
use Illuminate\Pagination\LengthAwarePaginator;
use SphinxQL;
use DB;
use Foolz\SphinxQL\Exception\DatabaseException as SphinxException;
use Weelis\Repository\Exceptions\RepositoryException;

trait SphinxSearch
{
    private $sphinx;
    private $call_sphinx;
    private $alias;
    private $aggregate;
    private $operation;
    private $multi;
    private $limited;
    private $sphinx_attrs;

    /**
     * Specify Model class name
     *
     * @return string
     */
    abstract public function sphinxIndex();

    public function sphinxMake()
    {
	    if(!isset($this->sphinx) || !($this->sphinx instanceof \Foolz\SphinxQL\SphinxQL)) {
		    $this->sphinxReset();
	    }
	    $this->call_sphinx = true;
        return $this->sphinx;
    }

	/**
	 * Search by single conditions
	 *
	 * @param        $condition
	 * @param        $field
	 * @param null   $value
	 * @param string $operator
	 *
	 * @return $this
	 * @throws SphinxException
	 */
    public function sphinxCondition($condition, $field, $value=null, $operator='=')
    {
	    if(is_string($value) && empty($value)) {
		    return $this;
	    }
	    // Avoid syntax error
        if(isset($value)) {
	        if (mb_strtolower($condition) == 'match') {
		        if(is_array($value)) {
			        $vals = array_map([$this, 'wrapEscape'], $value);
			        $this->sphinx = $this->sphinxMake()
				        ->match($field, \Foolz\SphinxQL\SphinxQL::expr(implode("|", $vals)));
		        } else {
			        $this->sphinx = $this->sphinxMake()
				        ->match($field, $value, $operator);
		        }
	        } elseif (mb_strtolower($condition) == 'in') {
		        if(!is_array($value)) {
			        throw new SphinxException('$value must be array "IN" condition');
		        }
		        $vals = implode(',', array_map('intval', $value));
		        $this->sphinxOperationSearch("IN($field, $vals)");
	        }
	        else {
		        $this->sphinx = $this->sphinxMake()
			        ->{$condition}($field, $operator, $value);
	        }
        }

        return $this;
    }

	/**
	 * Search by multiple condition excepted match
	 *
	 * @param        $where
	 * @param string $operation
	 *
	 * @return $this
	 */
	public function sphinxConditions($where, $operation = 'and')
	{
		// Avoid syntax error
		$conditions = [];
		foreach ($where as $field => $value) {
			if (is_array($value)) {
				list($field, $condition, $val) = $value;
				if(mb_strtolower($condition) == 'in') {
					if(is_array($val)) {
						$vals = implode(',', array_map('intval', $val));
						$conditions[] = "IN($field, $vals)";
					} else {
						$val = intval($val);
						$conditions[] = "IN($field, $val)";
					}
				}
				else {
					$val = $this->wrapQuote($val);
					$conditions[] = "IF($field $condition $val, 1, 0)";
				}
			} else {
				$val = $this->wrapQuote($value);
				$conditions[] = "IF($field = $val, 1, 0)";
			}
		}
		$make = '('.implode(" $operation ", $conditions).')';
		$alias = 'condition'.($this->alias++);

		$this->sphinx = $this->sphinxMake()
			->customColumns("$make as $alias")
			->where($alias, 1);

		return $this;
	}

    /**
     * Allow create custom operation sphinxql search
     * @param $operation
     * @return $this
     */
    public function sphinxOperationSearch($operation)
    {
        $operation_alias = 'operation'.($this->operation++);
        $this->sphinx = $this->sphinxMake()
            ->customColumns("$operation as $operation_alias")
            ->where($operation_alias, 1);
        return $this;
    }

    /**
     * Search distance radius
     * @param $field_long
     * @param $field_lat
     * @param $long
     * @param $lat
     * @param $distance meter
     * @return $this
     */
    public function sphinxGeoDistanceSearch($field_long, $field_lat, $long, $lat, $distance)
    {
        $distance_alias = 'distance'.($this->alias++);
        $this->sphinx = $this->sphinxMake()
            ->customColumns("SQRT(69.1*69.1*($field_lat - $lat)*($field_lat - $lat) + 53*53*($field_long - $long)*($field_long - $long)) * 1609 as $distance_alias")
//            ->customColumns("GEODIST($lat, $long, $field_lat, $field_long) as $distance_alias")
            ->where($distance_alias, '<=', $distance);
        return $this;
    }

	public function sphinxGeoOrderBy($field_long, $field_lat, $long, $lat, $order="ASC")
	{
		$distance_alias = 'distance'.($this->alias++);
		$this->sphinx = $this->sphinxMake()
			->customColumns("SQRT(69.1*69.1*($field_lat - $lat)*($field_lat - $lat) + 53*53*($field_long - $long)*($field_long - $long)) * 1609 as $distance_alias")
//            ->customColumns("GEODIST($lat, $long, $field_lat, $field_long) as $distance_alias")
			->orderBy($distance_alias, $order);
		return $this;
	}

	/**
	 * Search geo point in array distance
	 *
	 * @param      $array_field
	 * @param      $field_long
	 * @param      $field_lat
	 * @param      $field_distance
	 * @param      $long
	 * @param      $lat
	 * @param bool $empty_true
	 *
	 * @return $this
	 */
    public function sphinxArrayGeoDistanceSearch($array_field, $field_long, $field_lat, $field_distance, $long, $lat, $empty_true = true)
    {
        $distance_alias = 'distance'.($this->alias++);
        $operation = "any((x.$field_distance * 1000) >= (SQRT(69.1*69.1*(x.$field_lat - $lat)*(x.$field_lat - $lat) + 53*53*(x.$field_long - $long)*(x.$field_long - $long)) * 1609) for x in $array_field)";
        if($empty_true)
        {
            $operation .= " OR LENGTH($array_field)=0";
        }
        $this->sphinx = $this->sphinxMake()
            ->customColumns("$operation as $distance_alias")
            ->where($distance_alias, 1);
        return $this;
    }

	/**
	 * Search distance inside json field
	 *
	 * @param        $json_field
	 * @param        $field_long
	 * @param        $field_lat
	 * @param array  $range
	 *
	 * @param string $operation
	 *
	 * @return $this
	 */
    public function sphinxJsonGeoDistanceSearch($json_field, $field_long, $field_lat, array $range, $operation = 'or')
    {
	    $distance_alias = 'distance'.($this->alias++);
	    $conditions = [];
	    foreach ($range as $value) {
		    list($long, $lat, $distance) = $value;
		    $conditions[] = "(SQRT(69.1*69.1*(x.$field_lat - $lat)*(x.$field_lat - $lat) + 53*53*(x.$field_long - $long)*(x.$field_long - $long)) * 1609) <= ($distance * 1000)";
	    }
	    $make = implode(" $operation ", $conditions);
	    $this->sphinx = $this->sphinxMake()
		    ->customColumns("any($make for x in $json_field) as $distance_alias")
		    ->where($distance_alias, 1);
	    return $this;
    }

	/**
	 * Search inside json column
	 *
	 * @param        $json_field
	 * @param        $where
	 * @param string $comparison
	 * @param string $operation
	 *
	 * @return $this
	 */
    public function sphinxJsonWhere($json_field, $where, $comparison = 'any', $operation = 'and') {
	    $alias = 'json'.($this->alias++);
	    $conditions = [];
	    foreach ($where as $field => $value) {
		    if (is_array($value)) {
			    list($field, $condition, $val) = $value;
			    if(mb_strtolower($condition) == 'in') {
				    if(is_array($val)) {
					    $vals = implode(',', array_map([$this, 'wrapQuote'], $val));
					    $conditions[] = "IN(x.$field, $vals)";
				    } else {
					    $val = $this->wrapQuote($val);
					    $conditions[] = "IN(x.$field, $val)";
				    }
			    }
			    elseif(mb_strtolower($condition) == 'mid') {
				    $val = $this->wrapQuote($val);
				    $conditions[] = "(x.{$field}[0] <= $val and x.{$field}[1] >= $val)";
			    }
			    else {
				    $conditions[] = "x.$field $condition $val";
			    }
		    } else {
			    $val = $this->wrapQuote($value);
			    $conditions[] = "x.$field = $val";
		    }
	    }
	    $make = implode(" $operation ", $conditions);
	    $operation = "$comparison($make for x in $json_field)";

	    $this->sphinx = $this->sphinxMake()
		    ->customColumns("$operation as $alias")
		    ->where($alias, 1);
	    return $this;
    }

    /**
     * Add custom field operation in sphinxsearch
     *
     * @param      $operation
     * @param      $alias
     * @param bool $model_attribute
     *
     * @return $this
     */
    public function sphinxCustomField($operation, $alias)
    {
        $this->sphinx = $this->sphinxMake()
            ->customColumns("$operation as $alias");
        $this->sphinx_attrs[] = $alias;
        return $this;
    }

    public function sphinxOrderBy($column, $direction = null)
    {
        // Avoid syntax error
        $this->sphinx = $this->sphinxMake()
            ->orderBy($column, $direction);

        return $this;
    }

    public function sphinxWithinGroupOrderBy($column, $direction = null)
    {
        // Avoid syntax error
        $this->sphinx = $this->sphinxMake()
            ->withinGroupOrderBy($column, $direction);

        return $this;
    }

    public function sphinxGroupBy($column)
    {
        // Avoid syntax error
        $this->sphinx = $this->sphinxMake()
            ->groupBy($column);

        return $this;
    }

    public function sphinxOption($name, $value)
    {
        // Avoid syntax error
        $this->sphinx = $this->sphinxMake()
            ->option($name, $value);

        return $this;
    }

    public function sphinxLimit($limit, $offset=0)
    {
        // Avoid multiple limit
        $this->limited = true;

        $this->sphinx = $this->sphinxMake()
            ->limit($offset, $limit);

        return $this;
    }

    /**
     * Support Multi-Query
     * @return mixed
     */
    public function sphinxEnqueue()
    {
        $this->multi = true;
        $this->sphinx = $this->sphinxMake()
            ->enqueue()
            ->select()
            ->from($this->sphinxIndex());

        return $this;
    }

    // Apply sphinx before query
    protected function applyScope() {
	    // Sphinx function called
        if(isset($this->call_sphinx) && $this->call_sphinx) {
	        $sphinx = $this->sphinxMake();
            $founds = [];
            if(!$this->limited) {
                // Fix sphinxsearch 20 records limited error
                $this->sphinxLimit(config('repository.sphinxsearch.max_matches'));
            }

            if($this->multi)
            {
                $results = $sphinx->executeBatch();
                foreach ($results as $result)
                {
                    $founds = array_merge($founds, SphinxQL::with($result)->get());
                }
            } else {
                $founds = SphinxQL::with($sphinx->execute())->get();
            }
            $this->sphinxReset();

            $builder = $this->getModel();
            $model = $builder->getModel();
            $founds = array_map('intval', $founds);
            if(method_exists($model, 'getAutoIdName')) {
                $key_name = $model->getAutoIdName();
            } else {
                $key_name = $model->getKeyName();
            }
	        $this->model = $builder->whereIn($key_name, $founds);
            // Sort by found id for mysql only
            if(count($founds) > 0 && method_exists($builder, 'orderByRaw'))
            {
                $found_str = implode(',', $founds);
                $this->model = $builder->orderByRaw(DB::raw("FIELD(id, $found_str)"));
            }
        }

        return parent::applyScope();
    }

	/**
	 * Search when condition apply
	 *
	 * @param $condition
	 * @param $callback
	 * @return $this
	 *
	 */
	public function sphinxWhen($condition, $callback) {
		if($condition) {
			return $callback($this);
		}
		return $this;
	}


	/**
	 * Aggregate custom fields sphinxsearch
	 *
	 * @param bool $reset
	 *
	 * @return mixed
	 */
    public function sphinxAggregate($reset = true)
    {
	    $this->aggregate = true;
        $founds = SphinxQL::with($this->sphinxMake()->execute())->getByColumns($this->sphinx_attrs);

	    if($reset) {
		    $this->sphinxReset();
	    }
        return $founds;
    }

	/**
	 * Set return aggregate custom fields sphinxsearch
	 *
	 * @param bool $boolean
	 *
	 * @return mixed
	 */
    public function sphinxSetAggregate($boolean = false)
    {
		$this->aggregate = $boolean;
	    return $this;
    }

	/**
	 * Reset sphinx variable
	 *
	 * @return $this
	 */
    public function sphinxReset()
    {
	    $this->alias = 0;
	    $this->operation = 0;
	    $this->call_sphinx = false;
	    $this->aggregate = false;
	    $this->multi = false;
	    $this->limited = false;
	    $this->sphinx_attrs = ['id']; // Default document id
	    $this->sphinx = SphinxQL::query()
		    ->select()
		    ->from($this->sphinxIndex());
	    return $this;
    }

	/**
	 * Override parent reset
	 */
    public function reset()
    {
	    parent::reset();
	    return $this->sphinxReset();
    }

	/**
     * @param null $limit
     * @param array $columns
     * @param $method
     * @return mixed
     * @throws RepositoryException
     */
    public function sphinxPaginate($limit = null, $columns = ['*'], $method = LengthAwarePaginator::class)
    {
        if (!class_exists($method)) {
            throw new RepositoryException("Class {$method} not found");
        }
        if ($this->multi) {
            throw new RepositoryException("Can't paginate with multi query");
        }

        $limit = request()->get('perPage', is_null($limit) ? config('repository.pagination.limit', 15) : $limit);
        $page = request()->has('start')? (request()->get('start')/$limit+1) : request()->get('page', 1);
        $offset = ($page - 1) * $limit;
        $this->sphinxLimit($limit, $offset);

	    if($this->aggregate)
	    {
		    $results = $this->sphinxAggregate(false);
		    $meta = Helper::create($this->sphinxMake()->getConnection())->showMeta()->execute();
		    $total = 0;
		    foreach ($meta as $m)
		    {
			    if($m['Variable_name'] == 'total_found')
			    {
				    $total = intval($m['Value']);
				    break;
			    }
		    }
			$paginate = new $method($results, $total, $limit, $page);
		    $this->sphinxReset();
		    return $this->parserResult($paginate);
	    }

        $this->applyCriteria();
        $this->applyScope();

        $model = $this->getModel();
        $results = $model->get($columns);
	    // Get sphinxsearch meta
	    $meta = Helper::create($this->sphinxMake()->getConnection())->showMeta()->execute();
	    $total = 0;
	    foreach ($meta as $m)
	    {
		    if($m['Variable_name'] == 'total_found')
		    {
			    $total = intval($m['Value']);
			    break;
		    }
	    }

        if($total > 0) {
            // Using sphinx if found
            $results = new $method($results, $total, $limit, $page);
        } else {
            $results = $model->paginate($limit, $columns, 'page', $page);
        }

        $results->appends(request()->query());
        $this->reset();

        return $this->parserResult($results);
    }

	/**
	 * Override parent paginate
	 *
	 * @param null   $limit
	 * @param array  $columns
	 * @param string $method
	 *
	 * @return mixed
	 */
    public function paginate($limit = null, $columns = ['*'], $method = "paginate")
    {
	    if($this->call_sphinx) {
		    return $this->sphinxPaginate($limit, $columns, LengthAwarePaginator::class);
	    }
        return parent::paginate($limit, $columns, $method);
    }

    /**
     * Insert values into sphinxsearch
     *
     * @param array $array
     *
     * @return mixed
     * @throws \Foolz\SphinxQL\Exception\DatabaseException
     */
    public function sphinxInsert($array = [])
    {
        if( !isset($array['id']) ) {
            throw new SphinxException('Required id to insert sphinxsearch');
        }
        try {
            $qrepl = SphinxQL::query()->insert()->into($this->sphinxIndex());
            $qrepl->set($array)->execute();
        } catch (SphinxException $e)
        {
            // Reconnecting
            $qrepl = SphinxQL::reconnect()->insert()->into($this->sphinxIndex());
            $qrepl->set($array)->execute();
        }
        return $this;
    }

    /**
     * Update values into sphinxsearch
     * Replace values if existed
     *
     * @param array $array
     *
     * @return mixed
     * @throws \Foolz\SphinxQL\Exception\DatabaseException
     */
    public function sphinxUpdate($array = [])
    {
        if( !isset($array['id']) ) {
            throw new SphinxException('Required id to update sphinxsearch');
        }
        try {
            $qrepl = SphinxQL::query()->replace()->into($this->sphinxIndex());
            $qrepl->set($array)->execute();
        } catch (SphinxException $e)
        {
            // Reconnecting
            $qrepl = SphinxQL::reconnect()->replace()->into($this->sphinxIndex());
            $qrepl->set($array)->execute();
        }
        return $this;
    }

    /**
     * Delete record with id from sphinxsearch
     *
     * @param $id
     *
     * @return mixed
     * @throws \Foolz\SphinxQL\Exception\DatabaseException
     */
    public function sphinxDelete($id)
    {
        if( intval($id) <= 0 ) {
            throw new SphinxException('Required id to delete sphinxsearch');
        }
        try {
            SphinxQL::query()->delete()->from($this->sphinxIndex())->where('id', $id)->execute();
        } catch (SphinxException $e)
        {
            // Reconnecting
            SphinxQL::reconnect()->delete()->from($this->sphinxIndex())->where('id', $id)->execute();
        }
        return $this;
    }

    /**
     * Truncate sphinx index
     *
     * @return mixed
     */
    public function sphinxTruncate()
    {
        $rt_index = $this->sphinxIndex();
        try {
            SphinxQL::query()->query("TRUNCATE RTIndex $rt_index")->execute();
        } catch (SphinxException $e)
        {
            // Reconnecting
            SphinxQL::reconnect()->query("TRUNCATE RTIndex $rt_index")->execute();
        }
        return $this;
    }

    /**
     * Build excerpts
     *
     * @param        $messages
     * @param        $q
     *
     * @param string $start_tag
     * @param string $end_tag
     *
     * @param int    $limit
     *
     * @return \Foolz\SphinxQL\Drivers\ResultSetInterface
     */
    public function sphinxBuildExcerpt($messages, $q, $start_tag = '<b>', $end_tag = '</b>', $limit = 5)
    {
        $rt_index = $this->sphinxIndex();
        if(is_array($messages)) {
            $message_match = "(".implode(",", $messages).")";
        } else {
            $message_match = $messages;
        }
        $options = ['before_match' => $start_tag,
                    'after_match' => $end_tag,
                    'around' => $limit,
                    'use_boundaries' => 1,
                    'exact_phrase' => 1];
        try {
            $hits = Helper::create($this->sphinxMake()->getConnection())->callSnippets($message_match, $rt_index, $q, $options)->execute();
	        if(isset($hits))
            {
                return array_flatten($hits->fetchAllAssoc());
            }
            return null;
        } catch (SphinxException $e)
        {
            // Reconnecting
            $hits = Helper::create($this->sphinxMake()->getConnection())->callSnippets($message_match, $rt_index, $q, $options)->execute();
            if(isset($hits))
            {
                return array_flatten($hits->fetchAllAssoc());
            }
            return null;
        }
    }

    private function wrapQuote($tag) {
        return SphinxQL::query()->getConnection()->quote($tag);
    }

	private function wrapEscape($tag) {
		return '"'.addslashes($tag).'"';
	}
}