<?php

namespace Weelis\Repository\Traits;


use Cviebrock\EloquentSluggable\Services\SlugService;

trait SluggableEnable
{
	/**
	 * Find model by string
	 *
	 * @param        $string
	 * @param string $field_slug
	 * @param array  $columns
	 *
	 * @return mixed
	 */
	public function findByString($string, $field_slug = 'slug', $columns = ['*'])
	{
		$model_class = $this->model();
		$slug = SlugService::createSlug($model_class, $field_slug, $string, ['unique' => false]);
		$this->applyCriteria();
		$this->applyScope();
		if($this->getModel() instanceof $model_class)
		{
			$model = $this->getModel()->findBySlug($slug, $columns);
		} else {
			$model = $this->getModel()->where($field_slug, $slug)->first($columns);
		}

		$this->reset();
		if(isset($model)) {
			return $this->parserResult($model);
		}
		return $model;
	}

	/**
	 * Find model by slug string
	 *
	 * @param        $slug_string
	 * @param string $field_slug
	 * @param array  $columns
	 *
	 * @return mixed
	 */
	public function findBySlug($slug_string, $field_slug = 'slug', $columns = ['*'])
	{
		$this->applyCriteria();
		$this->applyScope();
		$model_class = $this->model();
		if($this->getModel() instanceof $model_class)
		{
			$model = $this->getModel()->findBySlug($slug_string, $columns);
		} else {
			$model = $this->getModel()->where($field_slug, $slug_string)->first($columns);
		}
		$this->reset();
		if(isset($model)) {
			return $this->parserResult($model);
		}
		return null;
	}
}