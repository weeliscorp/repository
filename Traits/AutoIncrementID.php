<?php

namespace Weelis\Repository\Traits;

/**
 * Using increase document id for m
 *
 * Class AutoIncrementID
 * @package Weelis\Repository\Traits
 */
trait AutoIncrementID
{
	/**
	 * Increment the counter and get the next sequence
	 *
	 * @param $collection
	 * @return mixed
	 */
	private static function getID($collection) {
		$seq = \DB::connection('mongodb')->getCollection('_data_counters')->findOneAndUpdate(
			array('_id' => $collection),
			array('$inc' => array('seq' => 1)),
			array('new' => true, 'upsert' => true, 'returnDocument' => \MongoDB\Operation\FindOneAndUpdate::RETURN_DOCUMENT_AFTER)
		);
		return $seq->seq;
	}

	/**
	 * Boot the AutoIncrementID trait for the model.
	 *
	 * @return void
	 */
	public static function bootAutoIncrementID() {
		static::creating(function ($model) {
			if(method_exists($model, 'getAutoIdName')) {
				$model->_auto_id = self::getID($model->getTable());
			}
		});
	}

	public function getAutoIdName() {
		return '_auto_id';
	}

	public function getAutoId() {
		return $this->_auto_id;
	}

	/**
	 * Get the casts array.
	 *
	 * @return array
	 */
	public function getCasts() {
		return $this->casts;
	}
}