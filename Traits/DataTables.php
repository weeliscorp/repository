<?php

namespace Weelis\Repository\Traits;


trait DataTables
{
	public function dataTable($filterFunc = null)
	{

		$limit = request()->get('length') > 1 ? (int)request()->get('length') : 0;
		if(isset($filterFunc) && is_callable($filterFunc)) {
			$filterFunc($this);
		}
		if($limit > 0) {
			$data = $this->presenter->present($this->paginate($limit));
			return [
				'data'            => $data['data'],
				'recordsTotal'    => $data['meta']['pagination']['total'],
				'recordsFiltered' => $data['meta']['pagination']['total'],
				'draw'            => (int)request()->get('draw')
			];
		}

		$data = $this->all()->presenter();
		$total = count($data['data']);
		return [
			'data'            => $data['data'],
			'recordsTotal'    => $total,
			'recordsFiltered' => $total,
			'draw'            => (int)request()->get('draw')
		];
	}
}