<?php namespace Weelis\Repository\Traits;

/**
 * Class TransformableTrait
 * @package Weelis\Repository\Traits
 */
trait TransformableTrait {

    /**
     * @return array
     */
    public function transform()
    {
        return $this->toArray();
    }

}