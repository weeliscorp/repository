<?php

namespace Weelis\Repository\Traits;

use Illuminate\Support\Arr;
use Weelis\Repository\Contracts\PresenterInterface;
use Weelis\Repository\Eloquent\PresenterCollection;
use Weelis\Repository\Eloquent\PresenterPaginator;

/**
 * Class PresentableTrait
 * @package Weelis\Repository\Traits
 */
trait PresentableTrait {

    /**
     * @var PresenterInterface
     */
    protected $presenter = null;

    /**
     * @param \Weelis\Repository\Contracts\PresenterInterface $presenter
     * @return $this
     */
    public function setPresenter(PresenterInterface $presenter){
        $this->presenter = $presenter;
        return $this;
    }

    /**
     * @return $this|mixed
     */
    public function presenter()
    {
        if( $this->hasPresenter() )
        {
            return $this->presenter->present($this);
        }

        return $this;
    }

    /**
     * @param $key
     * @param null $default
     * @return mixed|null
     */
    public function present($key, $default = null)
    {
        if ( $this->hasPresenter() )
        {
            $data = $this->presenter()['data'];
            return Arr::get($data, $key, $default);
        }

        return $default;
    }

    /**
     * @return bool
     */
    protected function hasPresenter()
    {
        return isset($this->presenter) && $this->presenter instanceof PresenterInterface;
    }

    /**
     * Create a new Presenter Collection instance.
     *
     * @param  array  $models
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function newCollection(array $models = [])
    {
        return new PresenterCollection($models);
    }
}
