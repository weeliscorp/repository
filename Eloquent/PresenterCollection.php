<?php
/**
 * Created by IntelliJ IDEA.
 * User: simonnguyen
 * Date: 3/31/16
 * Time: 7:57 PM
 */

namespace Weelis\Repository\Eloquent;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;
use Weelis\Repository\Contracts\PresenterInterface;

class PresenterCollection extends Collection
{
    /**
     * @var PresenterInterface
     */
    protected $presenter = null;

    /**
     * @param \Weelis\Repository\Contracts\PresenterInterface $presenter
     * @return $this
     */
    public function setPresenter(PresenterInterface $presenter){
        $this->presenter = $presenter;
        return $this;
    }

    /**
     * @return $this|mixed
     */
    public function presenter()
    {
        if( $this->hasPresenter() )
        {
            return $this->presenter->present($this);
        }

        return $this;
    }

    /**
     * @param $key
     * @param null $default
     * @return mixed|null
     */
    public function present($key, $default = null)
    {
        if ( $this->hasPresenter() )
        {
            $data = $this->presenter()['data'];
            return Arr::get($data, $key, $default);
        }

        return $default;
    }

    /**
     * @return bool
     */
    protected function hasPresenter()
    {
        return isset($this->presenter) && $this->presenter instanceof PresenterInterface;
    }


}