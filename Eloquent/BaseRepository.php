<?php
namespace Weelis\Repository\Eloquent;

use Closure;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Weelis\Repository\Contracts\CriteriaInterface;
use Weelis\Repository\Contracts\Presentable;
use Weelis\Repository\Contracts\PresenterInterface;
use Weelis\Repository\Contracts\RepositoryCriteriaInterface;
use Weelis\Repository\Events\RepositoryEntityCreated;
use Weelis\Repository\Events\RepositoryEntityDeleted;
use Weelis\Repository\Events\RepositoryEntityUpdated;
use Weelis\Repository\Exceptions\RepositoryException;
use Weelis\Repository\Validator\Contracts\ValidatorInterface;
use Weelis\Repository\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Container\Container as Application;
use Illuminate\Support\Collection;
use Weelis\Repository\Validator\Exceptions\ValidatorException;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * Class BaseRepository
 * @package Weelis\Repository\Eloquent
 */
abstract class BaseRepository implements RepositoryInterface, RepositoryCriteriaInterface
{

	/**
	 * @var Application
	 */
	protected $app;

	/**
	 * @var Model
	 */
	protected $model;

	/**
	 * @var array
	 */
	protected $fieldSearchable = [];

	/**
	 * @var PresenterInterface
	 */
	protected $presenter;

	/**
	 * @var ValidatorInterface
	 */
	protected $validator;

	/**
	 * Validation Rules
	 *
	 * @var array
	 */
	protected $rules = null;

	/**
	 * Collection of Criteria
	 *
	 * @var Collection
	 */
	protected $criteria;

	/**
	 * @var \Closure
	 */
	protected $transform;


	/**
	 * Skip generate update, delete, delete event
	 * @var bool
	 */
	protected $skipEvents = true;

	/**
	 * @var bool
	 */
	protected $skipCriteria = false;

	/**
	 * @var bool
	 */
	protected $skipPresenter = true;

	/**
	 * @var \Closure
	 */
	protected $scopeQuery = null;

	/**
	 * @param Application $app
	 */
	public function __construct(Application $app)
	{
		$this->app = $app;
		$this->criteria = new Collection();
		$this->transform = null;
		$this->makeModel();
		$this->makePresenter();
		$this->makeValidator();
		$this->boot();
	}

	/**
	 *
	 */
	public function boot()
	{

	}

	/**
	 * Get query builder
	 * @return mixed
	 */
	public function getQuery()
	{
		$this->applyCriteria();
		$this->applyScope();

		return $this->getModel();
	}

	/**
	 * Reset repository
	 * @return $this
	 */
	public function reset()
	{
		$this->resetModel();
		$this->resetScope();
		$this->resetCriteria();

		return $this;
	}

	/**
	 * @throws RepositoryException
	 */
	public function resetModel()
	{
		$this->makeModel();
	}

	/**
	 * Specify Model class name
	 *
	 * @return string
	 */
	abstract public function model();

	/**
	 * Get current model
	 * @return Model
	 */
	public function getModel()
	{
		if (!is_null($this->model)) {
			return $this->model;
		}

		return $this->makeModel();
	}

	/**
	 * Set transform results
	 *
	 * @param $transform
	 * @return $this
	 */
	public function setTransform($transform)
	{
		$this->transform = $transform;

		return $this;
	}

	/**
	 * Specify Presenter class name
	 *
	 * @return string
	 */
	public function presenter()
	{
		return null;
	}

	/**
	 * Specify Validator class name of Weelis\Validator\Contracts\ValidatorInterface
	 *
	 * @return null
	 * @throws Exception
	 */
	public function validator()
	{

		if (isset($this->rules) && !is_null($this->rules) && is_array($this->rules) && !empty($this->rules)) {
			if (class_exists('Weelis\Validator\LaravelValidator')) {
				$validator = app('Weelis\Validator\LaravelValidator');
				if ($validator instanceof ValidatorInterface) {
					$validator->setRules($this->rules);

					return $validator;
				}
			} else {
				throw new Exception(trans('repository::packages.prettus_laravel_validation_required'));
			}
		}

		return null;
	}

	/**
	 * Set Presenter
	 *
	 * @param $presenter
	 *
	 * @return $this
	 */
	public function setPresenter($presenter)
	{
		$this->makePresenter($presenter);

		return $this;
	}

	/**
	 * @return Model
	 * @throws RepositoryException
	 */
	public function makeModel()
	{
		$model = $this->app->make($this->model());

		if (!$model instanceof Model) {
			throw new RepositoryException("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");
		}

		return $this->model = $model;
	}

	/**
	 * @param null $presenter
	 *
	 * @return PresenterInterface
	 * @throws RepositoryException
	 */
	public function makePresenter($presenter = null)
	{
		$presenter = !is_null($presenter) ? $presenter : $this->presenter();

		if (!is_null($presenter)) {
			$this->presenter = is_string($presenter) ? $this->app->make($presenter) : $presenter;

			if (!$this->presenter instanceof PresenterInterface) {
				throw new RepositoryException("Class {$presenter} must be an instance of Weelis\\Repository\\Contracts\\PresenterInterface");
			}

			return $this->presenter;
		}

		return null;
	}

	/**
	 * @param null $validator
	 *
	 * @return null|ValidatorInterface
	 * @throws RepositoryException
	 */
	public function makeValidator($validator = null)
	{
		$validator = !is_null($validator) ? $validator : $this->validator();

		if (!is_null($validator)) {
			$this->validator = is_string($validator) ? $this->app->make($validator) : $validator;

			if (!$this->validator instanceof ValidatorInterface) {
				throw new RepositoryException("Class {$validator} must be an instance of Weelis\\Validator\\Contracts\\ValidatorInterface");
			}

			return $this->validator;
		}

		return null;
	}

	/**
	 * Get Searchable Fields
	 *
	 * @return array
	 */
	public function getFieldsSearchable()
	{
		return $this->fieldSearchable;
	}

	/**
	 * Query Scope
	 *
	 * @param \Closure $scope
	 *
	 * @return $this
	 */
	public function scopeQuery(\Closure $scope)
	{
		$this->scopeQuery = $scope;

		return $this;
	}

	/**
	 * Count model
	 * @return mixed
	 */
	public function count()
	{
		$this->applyCriteria();
		$this->applyScope();

		return $this->getModel()->count();
	}

	/**
	 * Retrieve data array for populate field select
	 *
	 * @param string $column
	 * @param string|null $key
	 *
	 * @return \Illuminate\Support\Collection|array
	 */
	public function lists($column, $key = null)
	{
		$this->applyCriteria();
		$this->applyScope();

		return $this->getModel()->lists($column, $key);
	}

	/**
	 * Retrieve all data of repository
	 *
	 * @param array $columns
	 *
	 * @return mixed
	 */
	public function all($columns = ['*'])
	{
		$this->applyCriteria();
		$this->applyScope();

		if ($this->getModel() instanceof Builder) {
			$results = $this->getModel()->get($columns);
		} else {
			$results = $this->getModel()->all($columns);
		}

		$this->reset();

		return $this->parserResult($results);
	}

	/**
	 * Get data of repository
	 *
	 * @param array $columns
	 *
	 * @return mixed
	 */
	public function get($columns = ['*'])
	{
		return $this->all($columns);
	}

	/**
	 * Get latest result
	 * @param array $columns
	 *
	 * @return mixed
	 */
	public function latest($columns = ['*'])
	{
		$this->applyCriteria();
		$this->applyScope();
		$results = $this->getModel()->orderBy('created_at', 'desc')->get($columns);

		$this->reset();

		return $this->parserResult($results);
	}


	/**
	 * Retrieve first data of repository
	 *
	 * @param array $columns
	 *
	 * @return mixed
	 */
	public function first($columns = ['*'])
	{
		$this->applyCriteria();
		$this->applyScope();

		$results = $this->getModel()->first($columns);

		$this->reset();

		return $this->parserResult($results);
	}

	/**
	 * Retrieve all data of repository, paginated
	 *
	 * @param null $limit
	 * @param array $columns
	 * @param string $method
	 *
	 * @return mixed
	 */
	public function paginate($limit = null, $columns = ['*'], $method = "paginate")
	{
		$this->applyCriteria();
		$this->applyScope();
		$limit = is_null($limit) ? config('repository.pagination.limit', 15) : $limit;
		$page = request()->has('start') ? (request()->get('start') / $limit + 1) : request()->get('page', 1);
		$results = $this->getModel()->{$method}($limit, $columns, 'page', $page);
		$results->appends(request()->query());
		$this->reset();

		return $this->parserResult($results);
	}

	/**
	 * Retrieve all data of repository, simple paginated
	 *
	 * @param null $limit
	 * @param array $columns
	 *
	 * @return mixed
	 */
	public function simplePaginate($limit = null, $columns = ['*'])
	{
		return $this->paginate($limit, $columns, "simplePaginate");
	}

	/**
	 * Find data by id
	 *
	 * @param       $id
	 * @param array $columns
	 *
	 * @return mixed
	 */
	public function find($id, $columns = ['*'])
	{
		$this->applyCriteria();
		$this->applyScope();
		$model = $this->getModel()->find($id, $columns);
		$this->reset();
		if ($model) {
			return $this->parserResult($model);
		}

		return $model;
	}

	/**
	 * Find data by field and value
	 *
	 * @param       $field
	 * @param       $value
	 * @param array $columns
	 *
	 * @return mixed
	 */
	public function findByField($field, $value = null, $columns = ['*'])
	{
		$this->applyCriteria();
		$this->applyScope();
		$model = $this->getModel()->where($field, $value)->get($columns);
		$this->reset();

		return $this->parserResult($model);
	}

	/**
	 * Find data by multiple fields
	 *
	 * @param array $where
	 * @param array $columns
	 *
	 * @return mixed
	 */
	public function findWhere(array $where, $columns = ['*'])
	{
		$this->applyCriteria();
		$this->applyScope();

		$this->applyConditions($where);

		$model = $this->getModel()->get($columns);
		$this->reset();

		return $this->parserResult($model);
	}

	/**
	 * Find data by multiple values in one field
	 *
	 * @param       $field
	 * @param array $values
	 * @param array $columns
	 *
	 * @return mixed
	 */
	public function findWhereIn($field, array $values, $columns = ['*'])
	{
		$this->applyCriteria();
		$model = $this->getModel()->whereIn($field, $values)->get($columns);
		$this->reset();

		return $this->parserResult($model);
	}

	/**
	 * Find data by excluding multiple values in one field
	 *
	 * @param       $field
	 * @param array $values
	 * @param array $columns
	 *
	 * @return mixed
	 */
	public function findWhereNotIn($field, array $values, $columns = ['*'])
	{
		$this->applyCriteria();
		$model = $this->getModel()->whereNotIn($field, $values)->get($columns);
		$this->reset();

		return $this->parserResult($model);
	}

	/**
	 * Save a new entity in repository
	 *
	 * @throws ValidatorException
	 *
	 * @param array $attributes
	 *
	 * @return mixed
	 */
	public function create(array $attributes)
	{
		if (!is_null($this->validator)) {
			// we should pass data that has been casts by the model
			// to make sure data type are same because validator may need to use
			// this data to compare with data that fetch from database.
			$attributes = $this->getModel()->newInstance()->forceFill($attributes)->makeVisible($this->getModel()->getFillable())->toArray();
			$this->validator->with($attributes)->passesOrFail(ValidatorInterface::RULE_CREATE);
		}

		$model = $this->getModel()->newInstance($attributes);
		$model->save();
		$this->reset();

		if (!$this->skipEvents) {
			event(new RepositoryEntityCreated($this, $model));
		}

		return $this->parserResult($model);
	}

	/**
	 * Update a entity in repository by id
	 *
	 * @throws ValidatorException
	 *
	 * @param array $attributes
	 * @param       $id
	 *
	 * @return mixed
	 */
	public function update(array $attributes, $id)
	{
		$this->applyCriteria();
		$this->applyScope();

		if (!is_null($this->validator)) {
			// we should pass data that has been casts by the model
			// to make sure data type are same because validator may need to use
			// this data to compare with data that fetch from database.
			$attributes = $this->getModel()->newInstance()->forceFill($attributes)->makeVisible($this->getModel()->getFillable())->toArray();

			$this->validator->with($attributes)->setId($id)->passesOrFail(ValidatorInterface::RULE_UPDATE);
		}

		$temporarySkipPresenter = $this->skipPresenter;

		$this->skipPresenter(true);
		$model = $this->getModel()->findOrFail($id);

		// Support meta
		if (method_exists($this, 'withMeta')) {
			$old = $this->withMeta()->find($id);
		} else {
			$old = clone $model;
		}

		$model->fill($attributes);
		$model->save();

		$this->skipPresenter($temporarySkipPresenter);
		$this->reset();

		if (!$this->skipEvents) {
			event(new RepositoryEntityUpdated($this, $model, $old));
		}

		return $this->parserResult($model);
	}

	/**
	 * Update a entity in repository by id
	 *
	 * @throws ValidatorException
	 *
	 * @param array $attributes
	 *
	 * @return mixed
	 */
	public function updateWhere(array $attributes)
	{
		$this->applyScope();

		if (!is_null($this->validator)) {
			// we should pass data that has been casts by the model
			// to make sure data type are same because validator may need to use
			// this data to compare with data that fetch from database.
			$attributes = $this->getModel()->newInstance()->forceFill($attributes)->makeVisible($this->getModel()->getFillable())->toArray();

			$this->validator->with($attributes)->passesOrFail(ValidatorInterface::RULE_UPDATE);
		}
		$result = $this->getModel()->update($attributes);
		$this->reset();

		return $result;
	}

	/**
	 * Update or Create an entity in repository
	 *
	 * @throws ValidatorException
	 *
	 * @param array $attributes
	 * @param array $values
	 *
	 * @return mixed
	 */
	public function updateOrCreate(array $attributes, array $values = [])
	{
		$this->applyScope();

		if (!is_null($this->validator)) {
			$this->validator->with($attributes)->passesOrFail(ValidatorInterface::RULE_UPDATE);
		}

		$temporarySkipPresenter = $this->skipPresenter;

		$this->skipPresenter(true);

		$model = $this->getModel()->updateOrCreate($attributes, $values);

		$this->skipPresenter($temporarySkipPresenter);
		$this->reset();

		if (!$this->skipEvents) {
			event(new RepositoryEntityUpdated($this, $model));
		}

		return $this->parserResult($model);
	}

	/**
	 * Delete a entity in repository by id
	 *
	 * @param $id
	 *
	 * @return int
	 */
	public function delete($id)
	{
		$this->applyScope();

		$temporarySkipPresenter = $this->skipPresenter;
		$this->skipPresenter(true);

		$model = $this->find($id);
		$originalModel = clone $model;

		$this->skipPresenter($temporarySkipPresenter);
		$this->reset();

		$deleted = $model->delete();

		if (!$this->skipEvents) {
			event(new RepositoryEntityDeleted($this, $originalModel));
		}

		return $deleted;
	}

	/**
	 * Delete multiple entities by given criteria.
	 *
	 * @param array $where
	 *
	 * @return int
	 */
	public function deleteWhere(array $where)
	{
		$this->applyScope();

		$temporarySkipPresenter = $this->skipPresenter;
		$this->skipPresenter(true);

		$this->applyConditions($where);

		$originalModels = $this->getModel()->get();
		foreach ($originalModels as $old) {
			if (!$this->skipEvents) {
				event(new RepositoryEntityDeleted($this, $old));
			}
			$old->delete();
		}

		$this->skipPresenter($temporarySkipPresenter);
		$this->resetModel();

		return true;
	}

	/**
	 * Check if entity has relation
	 *
	 * @param string $relation
	 *
	 * @return $this
	 */
	public function has($relation)
	{
		$this->model = $this->getModel()->has($relation);

		return $this;
	}

	/**
	 * Load relation has with closure
	 *
	 * @param string $relation
	 * @param closure $closure
	 *
	 * @return $this
	 */
	function whereHas($relation, $closure)
	{
		$this->model = $this->getModel()->whereHas($relation, $closure);

		return $this;
	}

	/**
	 * Check if entity doesn't have relation
	 *
	 * @param string $relation
	 *
	 * @return $this
	 */
	public function doesntHave($relation)
	{
		$this->model = $this->getModel()->doesntHave($relation);

		return $this;
	}

	/**
	 * Load relation doesn't have with closure
	 *
	 * @param string $relation
	 * @param closure $closure
	 *
	 * @return $this
	 */
	function whereDoesntHave($relation, $closure)
	{
		$this->model = $this->getModel()->whereDoesntHave($relation, $closure);

		return $this;
	}
	
	/**
	 * @param $condition
	 * @param $closure
	 * @return $this
	 */
	function when($condition, $closure)
	{
		$this->model = $this->getModel()->when($condition, $closure);

		return $this;
	}

	/**
	 * Load relations
	 *
	 * @param array|string $relations
	 *
	 * @return $this
	 */
	public function with($relations)
	{
		$this->model = $this->getModel()->with($relations);

		return $this;
	}

	/**
	 * Load relations count
	 *
	 * @param $relations
	 *
	 * @return $this
	 */
	public function withCount($relations)
	{
		$this->model = $this->getModel()->withCount($relations);

		return $this;
	}

	/**
	 * Load include soft-deleted model
	 *
	 *
	 * @return $this
	 */
	public function withTrashed()
	{
		$this->model = $this->getModel()->withTrashed();

		return $this;
	}

	/**
	 * Set hidden fields
	 *
	 * @param array $fields
	 *
	 * @return $this
	 */
	public function hidden(array $fields)
	{
		$this->getModel()->setHidden($fields);

		return $this;
	}

	public function orderBy($column, $direction = 'asc')
	{
		$this->model = $this->getModel()->orderBy($column, $direction);

		return $this;
	}

	/**
	 * Set visible fields
	 *
	 * @param array $fields
	 *
	 * @return $this
	 */
	public function visible(array $fields)
	{
		$this->getModel()->setVisible($fields);

		return $this;
	}

	/**
	 * Push Criteria for filter the query
	 *
	 * @param $criteria
	 *
	 * @return $this
	 * @throws \Weelis\Repository\Exceptions\RepositoryException
	 */
	public function pushCriteria($criteria)
	{
		if (is_string($criteria)) {
			$criteria = new $criteria;
		}
		if (!$criteria instanceof CriteriaInterface) {
			throw new RepositoryException("Class " . get_class($criteria) . " must be an instance of Weelis\\Repository\\Contracts\\CriteriaInterface");
		}
		$this->criteria->push($criteria);

		return $this;
	}

	/**
	 * Pop Criteria
	 *
	 * @param $criteria
	 *
	 * @return $this
	 */
	public function popCriteria($criteria)
	{
		$this->criteria = $this->criteria->reject(function ($item) use ($criteria) {
			if (is_object($item) && is_string($criteria)) {
				return get_class($item) === $criteria;
			}

			if (is_string($item) && is_object($criteria)) {
				return $item === get_class($criteria);
			}

			return get_class($item) === get_class($criteria);
		});

		return $this;
	}

	/**
	 * Get Collection of Criteria
	 *
	 * @return Collection
	 */
	public function getCriteria()
	{
		return $this->criteria;
	}

	/**
	 * Find data by Criteria
	 *
	 * @param CriteriaInterface $criteria
	 *
	 * @return mixed
	 */
	public function getByCriteria(CriteriaInterface $criteria)
	{
		$this->model = $criteria->apply($this->getModel(), $this);
		$results = $this->getModel()->get();
		$this->reset();

		return $this->parserResult($results);
	}

	/**
	 * Skip Criteria
	 *
	 * @param bool $status
	 *
	 * @return $this
	 */
	public function skipCriteria($status = true)
	{
		$this->skipCriteria = $status;

		return $this;
	}

	/**
	 * Reset all Criterias
	 *
	 * @return $this
	 */
	public function resetCriteria()
	{
		$this->criteria = new Collection();

		return $this;
	}

	/**
	 * Reset Query Scope
	 *
	 * @return $this
	 */
	public function resetScope()
	{
		$this->scopeQuery = null;

		return $this;
	}

	/**
	 * Apply scope in current Query
	 *
	 * @return $this
	 */
	protected function applyScope()
	{
		if (isset($this->scopeQuery) && is_callable($this->scopeQuery)) {
			$callback = $this->scopeQuery;
			$this->model = $callback($this->getModel());
		}

		return $this;
	}

	/**
	 * Apply criteria in current Query
	 *
	 * @return $this
	 */
	protected function applyCriteria()
	{

		if ($this->skipCriteria === true) {
			return $this;
		}

		$criteria = $this->getCriteria();

		if ($criteria) {
			foreach ($criteria as $c) {
				if ($c instanceof CriteriaInterface) {
					$this->model = $c->apply($this->getModel(), $this);
				}
			}
		}

		return $this;
	}

	/**
	 * Applies the given where conditions to the model.
	 *
	 * @param array $where
	 * @return void
	 */
	protected function applyConditions(array $where)
	{
		foreach ($where as $field => $value) {
			if (is_array($value)) {
				list($field, $condition, $val) = $value;
				if (mb_strtolower($condition) == 'in') {
					$this->model = $this->getModel()->whereIn($field, $val);
				} else {
					$this->model = $this->getModel()->where($field, $condition, $val);
				}
			} else {
				$this->model = $this->getModel()->where($field, $value);
			}
		}
	}

	/**
	 * Skip Presenter Wrapper
	 *
	 * @param bool $status
	 *
	 * @return $this
	 */
	public function skipPresenter($status = true)
	{
		$this->skipPresenter = $status;

		return $this;
	}

	/**
	 * Wrapper result data
	 *
	 * @param mixed $result
	 *
	 * @return mixed
	 */
	public function parserResult($result)
	{
		if ($this->presenter instanceof PresenterInterface) {
			if ($result instanceof Collection || $result instanceof LengthAwarePaginator) {
				$result->each(function ($model) {
					if ($model instanceof Presentable) {
						$model->setPresenter($this->presenter);
					}

					return $model;
				});
				if ($result instanceof Collection) {
					$result->setPresenter($this->presenter);
				}
			} elseif ($result instanceof Presentable) {
				$result = $result->setPresenter($this->presenter);
			}

			if (!$this->skipPresenter) {
				$results = $this->presenter->present($result);
				// Reset
				$this->skipPresenter = true;
				$this->makePresenter();

				return $results;
			}
		} else if (isset($this->transform) && is_callable($this->transform)) {
			if ($result instanceof Collection) {
				$result = $result->transform($this->transform);
			} elseif ($result instanceof LengthAwarePaginator) {
				$result->getCollection()->transform($this->transform);
			} else {
				$callback = $this->transform;
				$result = $callback($result);
			}
		}

		return $result;
	}
}
