<?php
namespace Weelis\Repository\Facades;
use Illuminate\Support\Facades\Facade;

class SphinxqlFacade extends Facade {
    protected static function getFacadeAccessor()
    {
        return 'sphinxql';
    }
}