<?php
namespace Weelis\Repository\Facades;

use Illuminate\Support\Facades\Facade;


class MetaRepo extends Facade {

    protected static function getFacadeAccessor() { return 'meta_repository'; }
}