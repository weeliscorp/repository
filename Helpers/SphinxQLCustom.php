<?php
namespace Weelis\Repository\Helpers;


class SphinxQLCustom extends \Foolz\SphinxQL\SphinxQL
{
    /**
     * Add the custom columns into select
     *
     * Gets the arguments passed as $sphinxql->customColumns('one', 'two')
     * Using it without arguments equals to having '*' as argument
     * Using it with array maps values as column names
     *
     * Examples:
     *    $query->customColumns('title');
     *    // SELECT title
     *
     *    $query->customColumns('title', 'author', 'date');
     *    // SELECT title, author, date
     *
     *    $query->customColumns(['id', 'title']);
     *    // SELECT id, title
     *
     * @param array|string $columns Array or multiple string arguments containing column names
     *
     * @return SphinxQL
     */
    public function customColumns($columns = null)
    {
        $this->type = 'select';
        if(empty($this->select)) {
            $this->select = ['*'];
        }
        if (is_array($columns)) {
            $this->select = array_merge($this->select, $columns);
        } else {
            $this->select = array_merge($this->select, \func_get_args());
        }

        return $this;
    }
}
