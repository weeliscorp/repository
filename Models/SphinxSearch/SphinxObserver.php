<?php

namespace Weelis\Repository\Models\SphinxSearch;


class SphinxObserver
{
    /**
     * Listen to the Model created event.
     *
     * @param $model
     */
    public function created($model)
    {
        $model->sphinxUpdate();
    }

    /**
     * Listen to the Model updated event.
     *
     * @param $model
     */
    public function updated($model)
    {
        $model->sphinxUpdate();
    }

    /**
     * Listen to the Model saved event.
     *
     * @param $model
     */
    public function saved($model)
    {
        $model->sphinxUpdate();
    }

    /**
     * Listen to the UsModeler deleting event.
     *
     * @param $model
     */
    public function deleting($model)
    {
        $model->sphinxDelete();
    }
}