<?php

namespace Weelis\Repository\Models\SphinxSearch;

use SphinxQL;
use Foolz\SphinxQL\Exception\DatabaseException as SphinxException;

trait SphinxSearchIndexer
{

    /**
     * Specify index name & index fields
     *
     * @return string
     */
    abstract public function searchableAs();
    abstract public function isIndex();
    abstract public function toSearchableArray();
    /**
     * Boot
     */
    protected static function bootSphinxSearchIndexer()
    {
        static::observe(SphinxObserver::class);
    }

    /**
     * Insert values into sphinxsearch
     *
     * @param array $array
     *
     * @return mixed
     * @throws \Foolz\SphinxQL\Exception\DatabaseException
     */
    public function sphinxInsert()
    {
        if($this->isIndex()) {
            try {
                $qrepl = SphinxQL::query()->insert()->into($this->searchableAs());
                $qrepl->set($this->toSearchableArray())->execute();
            } catch (SphinxException $e)
            {
                // Reconnecting
                $qrepl = SphinxQL::reconnect()->insert()->into($this->searchableAs());
                $qrepl->set($this->toSearchableArray())->execute();
            }
        }

        return $this;
    }

    /**
     * Update values into sphinxsearch
     * Replace values if existed
     *
     * @param array $array
     *
     * @return mixed
     * @throws \Foolz\SphinxQL\Exception\DatabaseException
     */
    public function sphinxUpdate()
    {
        if($this->isIndex()) {
            try {
                $qrepl = SphinxQL::query()->replace()->into($this->searchableAs());
                $qrepl->set($this->toSearchableArray())->execute();
            } catch (SphinxException $e) {
                // Reconnecting
                $qrepl = SphinxQL::reconnect()->replace()->into($this->searchableAs());
                $qrepl->set($this->toSearchableArray())->execute();
            }
        }
        return $this;
    }

    /**
     * Delete record with id from sphinxsearch
     *
     * @param $id
     *
     * @return mixed
     * @throws \Foolz\SphinxQL\Exception\DatabaseException
     */
    public function sphinxDelete()
    {
        if($this->isIndex()) {
            if (intval($this->getKey()) <= 0) {
                throw new SphinxException('Required id to delete sphinxsearch');
            }
            try {
                SphinxQL::query()->delete()->from($this->searchableAs())->where('id', $this->getKey())->execute();
            } catch (SphinxException $e) {
                // Reconnecting
                SphinxQL::reconnect()->delete()->from($this->searchableAs())->where('id', $this->getKey())->execute();
            }
        }
        return $this;
    }

    /**
     * Truncate sphinx index
     *
     * @return mixed
     */
    public function sphinxTruncate()
    {
        if($this->isIndex()) {
            $rt_index = $this->searchableAs();
            try {
                SphinxQL::query()->query("TRUNCATE RTIndex $rt_index")->execute();
            } catch (SphinxException $e) {
                // Reconnecting
                SphinxQL::reconnect()->query("TRUNCATE RTIndex $rt_index")->execute();
            }
        }
        return $this;
    }
}