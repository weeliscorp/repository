<?php
namespace Weelis\Repository\Events;

/**
 * Class RepositoryEntityCreated
 * @package Weelis\Repository\Events
 */
class RepositoryEntityCreated extends RepositoryEventBase
{
    /**
     * @var string
     */
    protected $action = "created";
}