<?php
namespace Weelis\Repository\Events;
use Illuminate\Database\Eloquent\Model;
use Weelis\Repository\Contracts\RepositoryInterface;

/**
 * Class RepositoryEntityDeleted
 * @package Weelis\Repository\Events
 */
class RepositoryEntityDeleted extends RepositoryEventBase
{
    /**
     * @var string
     */
    protected $action = "deleted";
}