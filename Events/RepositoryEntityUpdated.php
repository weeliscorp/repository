<?php
namespace Weelis\Repository\Events;

/**
 * Class RepositoryEntityUpdated
 * @package Weelis\Repository\Events
 */
class RepositoryEntityUpdated extends RepositoryEventBase
{
    /**
     * @var string
     */
    protected $action = "updated";
}