<?php
namespace Weelis\Repository\Events;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Queue\SerializesModels;
use Weelis\Repository\Contracts\RepositoryInterface;

/**
 * Class RepositoryEventBase
 * @package Weelis\Repository\Events
 */
abstract class RepositoryEventBase
{
    use SerializesModels;
    /**
     * @var Model
     */
    protected $model;

    /**
     * @var Model
     */
    protected $old_model;

    /**
     * @var RepositoryInterface
     */
    protected $repository;

    /**
     * @var string
     */
    protected $action;

    /**
     * @param RepositoryInterface $repository
     * @param Model $model
     * @param Model $old_model
     */
    public function __construct(RepositoryInterface $repository, Model $model, Model $old_model = null)
    {
        $this->repository = get_class($repository);
        $this->model = $model;
        if(isset($old_model))
        {
            $old_model = $old_model->toArray();
        } else {
            $old_model = $model->toArray();
        }
        $this->old_model = $old_model;
    }

    /**
     * Get updated model
     * @return Model
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Get before updated model
     * @return Model
     */
    public function getOldModel()
    {
        return $this->old_model;
    }

    /**
     * @return RepositoryInterface
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }
}